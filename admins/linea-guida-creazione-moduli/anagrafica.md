# Anagrafica



Di seguito vengono elencati i componenti **Anagrafica** disponibili che è possibile includere all'interno dei moduli:

I dati minimi strettamente necessari per le pratiche anonime:

* Codice Fiscale (`applicant.data.fiscal_code.data.fiscal_code`)

per utenti loggati viene fatto anche il controllo che i seguenti campi siano gli stessi dell'utente loggato:

* Nome (`applicant.data.completename.data.name`)
* Cognome (`applicant.data.completename.data.surname`)
* Codice Fiscale (`applicant.data.fiscal_code.data.fiscal_code`)

se si desidera abilitare l'invio di messaggi via e-mail, è necessario aggiungere all'anagrafica dell'applicant il campo apposito per l'email

* E-mail (`applicant.data.email_address`)

Se l'anagrafica non contiene la email, è necessario aggiungere nel form un campo con chiave `email_address` perché funzioni il meccanismo di avviso all'utente degli stati della pratica tramite mail.

### Anagrafica Istat <a href="#user-content-anagrafica-istat" id="user-content-anagrafica-istat"></a>

* in base al comune di residenza viene popolato un campo aggiuntivo con il codice istat del comune
* se la residenza è al di fuori dell'Italia si può specificare Provincia EE e compare un campo Nazione per indicare la propria nazione di residenza
* il numero di telefono può comprendere un prefisso internazionale

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/e033faee1a03097467ea560b3ca01def/image.png" alt=""><figcaption></figcaption></figure>

### Anagrafica <a href="#user-content-anagrafica" id="user-content-anagrafica"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/3a8553e513dc8e353b15be33e0b353ac/Schermata_2021-04-01_alle_16.20.44.png" alt=""><figcaption></figcaption></figure>

### Anagrafica lite <a href="#user-content-anagrafica-lite" id="user-content-anagrafica-lite"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/c3309e6ad3d050904ce6b09daf2ebe4c/Schermata_2021-04-01_alle_16.22.05.png" alt=""><figcaption></figcaption></figure>

### Anagrafica extra lite <a href="#user-content-anagrafica-extra-lite" id="user-content-anagrafica-extra-lite"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/c1237dc2de55454bf8d38650d7fbf40a/Schermata_2021-04-01_alle_16.23.09.png" alt=""><figcaption></figcaption></figure>

### Anagrafica minimale <a href="#user-content-anagrafica-minimale" id="user-content-anagrafica-minimale"></a>

&#x20;(NB: Ricordarsi che questa anagrafica non contiene il campo email, quindi sarà necessario aggiungere nel form un campo con chiave `email_address` per far funzionare il meccanismo di inoltro mail all'utente).

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/326403bcacc83a4a74188222454de294/Schermata_2021-04-01_alle_16.23.41.png" alt=""><figcaption></figcaption></figure>

### Anagrafica estesa <a href="#user-content-anagrafica-estesa" id="user-content-anagrafica-estesa"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/b1aa67e1cc442565e9e0c23c682605c5/Schermata_2021-04-01_alle_16.21.35.png" alt=""><figcaption></figcaption></figure>

### Anagrafica semplificata <a href="#user-content-anagrafica-semplificata" id="user-content-anagrafica-semplificata"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/e88f84ffea785763825c1af2e52b2ad9/Schermata_2021-04-01_alle_16.23.29.png" alt=""><figcaption></figcaption></figure>

### Anagrafica trentino <a href="#user-content-anagrafica-trentino" id="user-content-anagrafica-trentino"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/53b19f4c5a3867aaa4362724c860283d/Schermata_2021-04-01_alle_16.22.44.png" alt=""><figcaption></figcaption></figure>

### Anagrafica genitore FEM <a href="#user-content-anagrafica-genitore-fem" id="user-content-anagrafica-genitore-fem"></a>

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/4edf78d35475b7d6f152c8aab4b865dd/Schermata_2021-04-01_alle_16.22.15.png" alt=""><figcaption></figcaption></figure>

### Anagrafica contatti/Anagrafica OC

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/image (1).png" alt=""><figcaption><p>E' incluso un campo nascosto (API key: codice_catastale) che a seconda se è un paese estero o un comune italiano viene riempito con il codice catastale corretto.</p></figcaption></figure>
