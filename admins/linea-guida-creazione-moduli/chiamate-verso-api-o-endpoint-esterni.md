# Chiamate verso API o endpoint esterni

Requisiti minimi per un corretto funzionamento

1. Tutte le chiamate esterne devono avere il protocollo HTTPS
2. Abilitare i CORS a qualsiasi dominio oppure verso [https://www2.stanzadelcittadino.it/](https://www2.stanzadelcittadino.it/)
3. Dal modulo della pratica NON è possibile chiamare indirizzi arbitrari
