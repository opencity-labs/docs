---
description: >-
  L'integrazione con form.io ha reso molto flessibile la creazione di servizi,
  consentendo a un amministratore di creare in completa autonomia un servizio
  digitale e metterlo online in poche ore.
---

# Regole generali

Ci sono però alcune regole di base da seguire affinché la creazione del servizio abbia successo.

## Regola 0: l'Applicant

All'interno di un modulo di deve essere sempre un un'anagrafica nominata `Applicant`. Deve essere una delle Anagrafiche disponibili come _nested form_ definiti nel sistema.

* \[`MUST`] Quando si inserisce un componente di tipo **Nested Form** è necessario ricordarsi di togliere la spunta al checkbox **Save as reference** presente nel tab **Form**
* \[`MUST`] Non inserire componenti di tipo **Nested Form** all'interno di elementi di layout per il corretto popolamento automatico dei campi

## Regola 1: API value <a href="#user-content-regola-1-api-value" id="user-content-regola-1-api-value"></a>

Per ogni campo compilare sempre il valore di Property Name dell'API, come mostrato in questo esempio:

Questo nome è quello che risulterà anche nelle API quando si consulta con una GET una pratica (application) di quel servizio, per questo motivo, per questo motivo è importante rispettare questa convenzione:

1. un nome di tipo `snake_case` (parole separate da \_)
2. usare la lingua inglese

## Le date <a href="#user-content-le-date" id="user-content-le-date"></a>

il componente di form.io per le date è piuttosto avanzato ma può creare problemi di compatibilità con i browser più datati, per fare una form con alto grado di compatibilità consigliamo un campo **textfield** con input mask `99/99/9999` a cui aggiungiamo una validazione js

```
let passedDate = moment(data.<api>, 'DD/MM/YYYY');
if (!passedDate.isValid()){
  valid = "La data inserita non è valida";
}
if (passedDate.isAfter(moment())){
    valid = "La data inserita non può essere successiva alla data odierna";
}
```

Vedi anche [controlli avanzati sulle date](https://gitlab.com/opencontent/stanzadelcittadino/-/snippets/2055450)

## Pagamenti <a href="#user-content-pagamenti" id="user-content-pagamenti"></a>

Per poter inserire un pagamento in un form va inserito un campo di tipo text e rispettare le seguenti regole:

* \[`MUST`] Il name del campo deve essere `payment_amount`

### Causale di pagamento <a href="#user-content-causale-di-pagamento" id="user-content-causale-di-pagamento"></a>

È possibile personalizzare il valore della causale di pagamento calcolandone il valore all'interno del form inserendo un campo di tipo text e rispettando le seguenti regole:

* \[`MUST`] Il name del campo deve essere `payment_description`
* \[`MUST`] Il campo non deve superare i 60 caratteri per evitare che la causale venga troncata all'interno dell'avviso di pagamento pdf generato dal gateway di pagamento MyPay

Nel caso in cui non venga definita nel modulo la causale assumerà un valore di default pari a:

`<identificativo pratica> - <codice fiscale richiedente>`

## Componenti <a href="#user-content-componenti" id="user-content-componenti"></a>

#### Wizard <a href="#user-content-wizard" id="user-content-wizard"></a>

* \[`MUST`] Verificare nelle impostazioni delle pagine che non sia spuntato il campo Collapsible

#### Componenti nascosti <a href="#user-content-componenti-nascosti" id="user-content-componenti-nascosti"></a>

E'possibile nascondere un componente nell'anteprima e dalla stampa del form aggiungendo la classe css `d-preview-none`; durante la compilazione del form il componente resterà visibile.

#### Select popolato via API <a href="#user-content-select-popolato-via-api" id="user-content-select-popolato-via-api"></a>

* \[`SHOULD`] Quando si inserisce un componente di tipo **select** è buona norma popolare i campi `Data Path` (se presente) e `Value Property` in modo da popolare anche il campo `value` della select.

#### Nested form <a href="#user-content-nested-form" id="user-content-nested-form"></a>

* \[`MUST`] Quando si inserisce un componente di tipo **Nested Form** è necessario ricordarsi di togliere la spunta al checkbox **Save as reference** presente nel tab **Form**
* \[`MUST`] Non inserire componenti di tipo **Nested Form** all'interno di elementi di layout per il corretto popolamento automatico dei campi

#### Upload dei file <a href="#user-content-upload-dei-file" id="user-content-upload-dei-file"></a>

Il componente per l'upload del file è utilizzabile cone le seguenti impostazioni:

* Storage va impostato a `Url`
* Va specificata un url così composto `https://{host}/{instance}/allegati` Es.`http://stanzadelcittadino.it/comune-di-bugliano/allegati`

Se viene specificando in fase di impostazione il tipo di file (tab file del componente upload), questo valore viene utilizzato come descrizione del file

## Validazione campo recapito telefonico <a href="#user-content-validazione-campo-recapito-telefonico" id="user-content-validazione-campo-recapito-telefonico"></a>

Usare un componente TextField ed aggiungere in validation sotto la voce "Regular Expression Pattern " il seguente `^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$`

## Impedire l'invio di più pratiche per un servizio <a href="#user-content-impedire-linvio-di-piu-pratiche-per-un-servizio" id="user-content-impedire-linvio-di-piu-pratiche-per-un-servizio"></a>

In alcuni casi è necessario ricevere per un servizio una sola pratica per ogni cittadino, oppure per ogni impresa oppure per ogni nucleo familiare. Un modo di risolvere questa necessità è l'aggiunta di un campo specifico al modulo sul quale il sistema fa un controllo di univocità. Il campo deve avere nome:

`unique_id`

Il contenuto deve essere il valore univoco su cui fare il controllo

### Pagamento con Bilancio PagoPA - Versione 1 (Mypay) <a href="#user-content-pagamento-con-bilancio-pagopa-versione-1-mypay" id="user-content-pagamento-con-bilancio-pagopa-versione-1-mypay"></a>

Il componente bilancio serve per poter dividere l'ammontare dei pagamenti in sottovoci. Per ogni riga di bilancio possono essere specificati:

* Codice del capitolo \[`MUST`]
* Codice dell'ufficio \[`MUST`]
* Codice di accertamento
* Importo \[`MUST`]

Il componete di bilancio è completamente trasparente all'utente finale, deve essere quindi impostato come `hidden` e deve rispettare le seguenti regole:

* \[`MUST`] Il name del campo deve essere `payment_financial_report`
* \[`MUST`] La somma degli importi della componente di bilancio deve essere uguale al `payment_amount` specificato
* \[`MUST`] L'importo all'interno della riga di bilancio deve essere maggiore di 0

Nel caso di componenti di bilancio complesse dipendenti da altri campi del form va inserito il valore delle componenti di bilancio dinamicamente tramite funzione js.

Nel `Calculated value` del campo Bilancio va inserita una funzione come l'esempio sottostante, tale funzione viene attivata al cambiamento (redraw) del campo collegato (tipologia).

Es.

```javascript
if (data.tipologia === 'value') {
 	value = [
 		{codAccertamento: "", codCapitolo: "cap1", codUfficio: "uff1", importo: "2"},
 		{codAccertamento: "", codCapitolo: "cap2", codUfficio: "uff2", importo: "8"}
 	];
 } else {
 	value = [
 		{codAccertamento: "", codCapitolo: "cap1", codUfficio: "uff1", importo: "5"},
 		{codAccertamento: "", codCapitolo: "cap2", codUfficio: "uff2", importo: "5"}
 	];
 }
```

### Pagamento con Bilancio PagoPA - Versione 2 (Mypay 2, Efil, Iris, PmPay) <a href="#user-content-pagamento-con-bilancio-pagopa-versione-2-mypay-2-efil-iris-pmpay" id="user-content-pagamento-con-bilancio-pagopa-versione-2-mypay-2-efil-iris-pmpay"></a>

Con l'implementazione dei nuovi proxy di pagamento la suddivisione degli importi in caso di pagamento viene gestita direttamente nel tab del pagamento della configurazione del servizio.

Nel caso quindi di un bilancio che non varia dinamicamente al variare del modulo non è più necessario inserire il componente bilancio nel modulo.

La configurazione è specifica per ogni proxy, l'interfaccia proposta può quindi variare in base al proxy utilizzato.

&#x20;(Es. configurazione di bilancio del Gateway Efil)

Nel caso di componenti di bilancio complesse dipendenti da altri campi del form va inserito il componente bilancio nel modulo il cui valore va personalizzato tramite funzione js.

* \[`MUST`] Il name del campo deve essere `payment_financial_report`
* \[`MUST`] Il componente va impostato come `hidden`
* \[`MUST`] La somma degli importi della componente di bilancio deve essere uguale al `payment_amount` specificato

Nel `Calculated value` del campo Bilancio va inserita una funzione come l'esempio sottostante, tale funzione viene attivata al cambiamento (redraw) del campo collegato (tipologia).

Es.

```javascript
if (data.tipologia === 'value') {
    value = {
      "2022/1": "14.00",
      "2022/2": "2.50"
    };
 } else {
    value = {
      "2022/1": null,
      "2022/2": "16.50"
    };
 }
```

La chiave dell'oggetto passato deve essere uguale all'id specificato in fase di configurazione.

## Altre informazioni utili <a href="#user-content-altre-informazioni-utili" id="user-content-altre-informazioni-utili"></a>

* [Cosa fare quando si cambia il nome di un campo in un modulo che ha già raccolto pratiche](https://gitlab.com/opencontent/stanzadelcittadino/-/snippets/2053049)

## Il valore della pratica <a href="#user-content-il-valore-della-pratica" id="user-content-il-valore-della-pratica"></a>

* Campo convenzionale: `application_value`

## Problemi noti <a href="#user-content-problemi-noti" id="user-content-problemi-noti"></a>

#### Una volta aggiunta una nuova scheda in un modulo non si riesce a rimuoverla facendo click sulla X rossa <a href="#user-content-una-volta-aggiunta-una-nuova-scheda-in-un-modulo-non-si-riesce-a-rimuoverla-facendo-cli" id="user-content-una-volta-aggiunta-una-nuova-scheda-in-un-modulo-non-si-riesce-a-rimuoverla-facendo-cli"></a>

Workaround: cancellare la tab con la X rossa, fare una modifica qualunque in un'altra scheda, salvare (facendo click sul tasto avanti) e la scheda verrà effettivamente rimossajava
