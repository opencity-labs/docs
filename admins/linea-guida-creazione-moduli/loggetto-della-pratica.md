---
description: L'oggetto viene utilizzato
---

# L'oggetto della pratica

L'oggetto viene generato automaticamente per ogni pratica, con:

`Nome del servizio (primi 255 caratteri + Nome e Cognome + Codice Fiscale del richiedente`

L'oggetto può essere sovrascritto usando il campo `application_subject` all'interno del modulo di form.io.

