# Pratiche geolocalizzati

### Definizione

Una pratica geolocalizzata è una pratica dotata di una informazione geografica. Dal punto di vista dei dati il **requisito minimo** è la presenza di un campo `address` contenente almeno una coordinata.

```
{
  "address": {
    "lat": "41.9057477",
    "lon": "12.4821294",
  }
}
```

Se disponibile può essere mostrata anche una descrizione testuale del luogo:

```
{
  "address": {
    "lat": "41.9057477",
    "lon": "12.4821294",
    "display_name": "Piazza di Spagna, Roma",
  }
}
```

Una descrizione completa può comprendere anche molti altri dati, tra cui la definizione puntuale di tutti gli elementi dell'indirizzo.

### Pratiche geolocalizzate a form.io

Se voglio geolocalizzare le pratiche di un servizio, è necessario aggiungere un campo di tipo `address` come da immagine. Questo elemento **deve** essere di primo livello, quindi non può essere contenuto da altri componenti, se non quelli di layout. Ad esempio **non** può essere inserito in un componente data, come un `Data Grid` o un `Container`, ma **può** essere contenuto in un elemento layout, come un `Panel` o un `Fieldset`).

<figure><img src="../.gitbook/assets/image (8).png" alt=""><figcaption></figcaption></figure>

Nella configurazione del campo è necessario impostare come "Provider" `OpenStreetMap Nominatim` (⚠️unico provider supportato) e come "API"->"Property Name" `address`.

* Esempio di uno schema completo per un campo address
* ```json
  {
              "label": "Indirizzo",
              "tableView": false,
              "provider": "nominatim",
              "key": "address",
              "type": "address",
              "providerOptions": {
                  "params": {
                      "autocompleteOptions": {}
                  }
              },
              "input": true,
              "components": [
                  {
                      "label": "Address 1",
                      "tableView": false,
                      "key": "address1",
                      "type": "textfield",
                      "input": true,
                      "customConditional": "show = _.get(instance, 'parent.manualMode', false);"
                  },
                  {
                      "label": "Address 2",
                      "tableView": false,
                      "key": "address2",
                      "type": "textfield",
                      "input": true,
                      "customConditional": "show = _.get(instance, 'parent.manualMode', false);"
                  },
                  {
                      "label": "City",
                      "tableView": false,
                      "key": "city",
                      "type": "textfield",
                      "input": true,
                      "customConditional": "show = _.get(instance, 'parent.manualMode', false);"
                  },
                  {
                      "label": "State",
                      "tableView": false,
                      "key": "state",
                      "type": "textfield",
                      "input": true,
                      "customConditional": "show = _.get(instance, 'parent.manualMode', false);"
                  },
                  {
                      "label": "Country",
                      "tableView": false,
                      "key": "country",
                      "type": "textfield",
                      "input": true,
                      "customConditional": "show = _.get(instance, 'parent.manualMode', false);"
                  },
                  {
                      "label": "Zip Code",
                      "tableView": false,
                      "key": "zip",
                      "type": "textfield",
                      "input": true,
                      "customConditional": "show = _.get(instance, 'parent.manualMode', false);"
                  }
              ]
          }
  ```
* Esempio di JSON presente in una pratica che contiene un campo di questo tipo
* ```json
  {
          "address": {
              "place_id": 113802302,
              "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
              "osm_type": "way",
              "osm_id": 26784878,
              "boundingbox": [
                  "41.9048494",
                  "41.9065125",
                  "12.4812423",
                  "12.4828533"
              ],
              "lat": "41.9057477",
              "lon": "12.4821294",
              "display_name": "Piazza di Spagna, Campo Marzio, Municipio Roma I, Roma, Roma Capitale, Lazio, 00187, Italia",
              "class": "highway",
              "type": "pedestrian",
              "importance": 0.7409079419032848,
              "address": {
                  "road": "Piazza di Spagna",
                  "quarter": "Campo Marzio",
                  "suburb": "Municipio Roma I",
                  "city": "Roma",
                  "county": "Roma Capitale",
                  "ISO3166-2-lvl6": "IT-RM",
                  "state": "Lazio",
                  "ISO3166-2-lvl4": "IT-62",
                  "postcode": "00187",
                  "country": "Italia",
                  "country_code": "it"
              }
          }
  ```
