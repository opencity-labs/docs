---
description: >-
  L'SDK ha lo scopo di consentire alcuni tipi di chiamate in modo semplificato
  dai campi validation o campi calcolati dei moduli fatti con Form.io.
---

# Form.io SDK



Le funzioni disponibili sono accessibili da `window.FormioHelper`

### `authenticatedCall('applications'): Promise`

restituisce le pratiche dell'utente autenticato

#### Esempio: Settare le opzioni di una select

```
async function get_data() {
  window.FormioHelper.authenticatedCall("applications")
  .then(
    function(promise) {
      console.log(promise)
      instance.setItems(promise.data)
    })
}

if (values.length === 0) {
  get_data()
}
```

### `getTenantInfo(): Promise`

restituisce le info del tenant, in particolare è utile per reperire il tenant-id

#### Esempio:

```
async function check() {
  tenant = await window.FormioHelper.getTenantInfo()
  if (tenant.comune === data.applicant.data.address.data.municipality) {
    ...
  } else {
    ...
  }
}
```

oppure

```
window.FormioHelper.getTenantInfo().then(data => {...})
```

### `getCurrentLocale(): string`

restituisce la lingua corrente

#### Esempio:

```
lang = window.FormioHelper.getCurrentLang()
if (lang === "it") {
...
} else {
...
}
```

### `getRemoteJson(url, method = 'get', headers = null): Promise`

restiuisce un json da una API remota specificando eventualmente metodo e headers custom per la chiamata

#### Esempio: Settare le opzioni di una select

```
async function get_data() {
  window.FormioHelper.getRemoteJson('https://api.opencontent.it/geo/comuni')
  .then(
    function(promise) {
      console.log(promise)
      instance.setItems(promise.data)
    })
}

if (values.length === 0) {
  get_data()
}
```

oppure

```
window.FormioHelper.getRemoteJson('https://api.opencontent.it/geo/comuni').then(data => {...})
```

#### Esempio: Recupero dato dai meta

Senza nessun parametro restituisce tutto l'oggetto `meta`

```
window.FormioHelper.getFieldMeta()
```

Con parametro restituisce il valore della chiave oggetto - primo livello

```
window.FormioHelper.getFieldMeta('legals')
```

Con parametro restituisce il valore della chiave oggetto - secondo livello

```
window.FormioHelper.getFieldMeta('legals.privacy_info')
```

Esempio con array

```
  window.FormioHelper.getFieldMeta('topics[0].text')
```

In caso il filtro non trovi gli elementi restituirà `false`
