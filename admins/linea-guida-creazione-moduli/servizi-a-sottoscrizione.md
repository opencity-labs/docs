# Servizi a sottoscrizione

###

Nel servizi a sottoscrizione sono disponibili due integrazioni:

* **Iscrizione al servizio** per l'iscrizione di un cittadino
* **Pagamenti per servizi a sottoscrizione** per registrare un pagamento collegato ad un'iscrizione **esistente**

## **Iscrizione al servizio**

Per creare un iscrizione ad un servizio a sottoscrizione è necessario che il modulo contenga i seguenti campi:

* Subform `applicant`: dati anagrafici del richiedente
* Campo `code`: codice del servizio a sottoscrizione

Oppure nel caso di iscrizione per conto di altri (esempio di un genitore che iscrive il figlio minorenne):

* Subform `subscriber`: dati anagrafici del richiedente
* Campo `code`: codice del servizio a sottoscrizione

Il subform `applicant`/`subscriber` dovrà fornire i campi:

```
"subscriber.data.completename.data.name",
"subscriber.data.completename.data.surname",
"subscriber.data.Born.data.natoAIl",
"subscriber.data.Born.data.place_of_birth",
"subscriber.data.fiscal_code.data.fiscal_code",
"subscriber.data.address.data.address",
"subscriber.data.address.data.house_number",
"subscriber.data.address.data.municipality",
"subscriber.data.address.data.postal_code",
"subscriber.data.email_address"
```

NOTE

* Nel caso in cui l'iscrizione preveda un pagamento verrà automaticamente salvato un pagamento associato all'iscrizione appena creata con una descrizione fissa **Quota di iscrizione** e nome **Quota di iscrizione - {nome corso} - {nome e cognome iscritto} {cf iscritto}**
* Il sistema consente un'unica iscrizione per corso, è dunque necessario bloccare iscrizioni duplicate durante la compilazione del modulo. A questo scopo è sufficiente effettuare una richiesta all'endpoint `https://{host}/{instance}/subscriptions/availability?code={code}&cf={fiscal_code}` dove
  * `code` è il codice del corso per il quale si vuole verificare l'esistenza di duplicati;
  * `fiscal_code` è il codice fiscale del cittadino che si desidera iscrivere (nota distinzione `applicant` e `subscriber` descritta nel paragrafo precedente)

La chiamata restituisce 400/406/200 a seconda della disponibilità dell'iscrizione.

Si rimanda alla validazione descritta nel paragrafo "Fascicoli".

## **Pagamenti per Servizi a sottoscrizione**

Per salvare un pagamento associato ad un iscrizione **esistente** è necessario che il modulo contenga i seguenti campi:

### **L'iscritto effettua il pagamento per se stesso**

* Subform `applicant`: dati anagrafici del richiedente (i.e. l'iscritto che effettua il pagamento)
* Campo `code`: codice del servizio a sottoscrizione
* Campo `payment_identifier`: identificativo del pagamento che verrà salvato come nome della voce di pagamento
* Campo `payment_amount`: importo del pagamento
* Campo `payment_reason`: Causale del pagamento che verrà salvata come descrizione della voce di pagamento
* Campo `unique_id`: Vincolo di univocità: questo campo serve nel caso in cui vengano create delle pratiche in bozza oppure l'import dei pagamenti tramite csv, in modo che non vengano create pratiche di pagamento duplicate

Il subform `applicant`/`subscriber` dovrà fornire i campi:

```
"subscriber.data.fiscal_code.data.fiscal_code"
```

Il campo `code` può essere sostituito con il campo `subscription_service` contenente l'identificativo del servizio a sottoscrizione

### **L'iscritto effettua il pagamento per un altro**

Nel caso in cui il richiedente effettua il pagamento per un iscritto deve essere presente il subform `subscriber` analogamente a quanto avviene per l'integrazione **Servizi a sottoscrizione**.

Anche in questo caso è possibile sostituire il campo `code` con il campo `subscription_service`

NOTE:

Affinche l'integrazione vada a buon fine una volta verificato il pagamento è indispensabile che esista un iscrizione per il cittadino con il codice fiscale inserito nel modulo. Nel caso contrario la pratica resterà bloccata nello stato precedente a quello indicato come punto di attivazione

## Prenotazione appuntamenti <a href="#user-content-prenotazione-appuntamenti" id="user-content-prenotazione-appuntamenti"></a>

Per aggangiare un servizio form.io al backoffice della prenotazione appuntamenti la form dovrà contenere almeno i seguenti campi:

* Subform `applicant`: dati anagrafici del richiedente: Non sono richiesti tutti i campi presenti nel componente/subform `anagrafica`, ma è sufficiente utilizzare la subform `anagrafica-lite` (`name`, `surname`, `email_address`, `phone_number` e `fiscal_code`)
* Campo `calendar`: il calendario per la scelta del giorno e dello slot disponibile. La compilazione di questo campo restituirà una stringa del tipo `d/m/Y @ H:i-H:i (calendar_id#meeting_id#opening_hour_id)`
* Campo `user_message`: il messaggio descrittivo dell'utente che prenota l'appuntamento

Si consiglia di utilizzare il form `Anagrafica-lite` a tale scopo.
