# Errore Unknown Component

## L'errore

Un errore che può capitare quando si fa l'editing dei servizi tramite il form builder è la comparsa di un "Unknown component", alla fine del form, prima dei pulsanti di invio/annulla.

<figure><img src="../.gitbook/assets/image (7).png" alt=""><figcaption></figcaption></figure>

### Il bug

L'errore è dovuto a un bug dell'editor, si può correggere, ma non da interfaccia web.

Un form di form.io in stanza del cittadino è sempre di tipo **wizard**, ovvero fatto da N pagine.

<figure><img src="../.gitbook/assets/image (28).png" alt=""><figcaption></figcaption></figure>

Un form di tipo wizard DEVE avere sempre una struttura fatta da N panels, una per ogni pagina che vedete all'interno dell'editor:

```json
{
"display": "wizard",
"type": "form",
"components": [
  {
     "title": "Richiedente",
     "type": "panel",
     ....
  },
  {
     "title": "Documenti",
     "type": "panel",
     ...
  },
  {
  {
     "title": "Consegna",
     "type": "panel",
     ...
  }
],
"_id": "61c58764fc3ee60017b98c21",
...
"__v": 11
}
```

Quando si verifica l'errore è perché sotto components c'e' qualcos'altro:

<figure><img src="../.gitbook/assets/image (18).png" alt=""><figcaption></figcaption></figure>

### La soluzione

Per risolvere bisogna procedere in questo modo:

1. si prende l'ID del form mediante API: `GET https://stanzadelcittadino.it/{tenant}/api/services/{service_id}`
2. si prende il JSON del form da correggere: `GET https://formserver.../form/{form_id}`
3. si mette in un editor di testo e si toglie la parte contenente l'errore, ovvero si lasciano solo le componenti di tipo `panel`
4. si fa una PUT sul formserver con il JSON corretto:

```
 cat /tmp/form-corretto.json | http put https://formserver.../{service-name}
```

N.B: la PUT va fatta sullo slug del servizio, non è disponibile sull'ID del form.
