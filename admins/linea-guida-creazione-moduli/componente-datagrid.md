# Componente Datagrid

Quando si usa il componente Datagrid ed accedere ai valori delle righe si deve utilizzare

`row.{{nome_input}}`

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

**Funzionalità avanzate** Per una corretta configurazione del componete Datagrid sono necessarie alcune osservazioni:

* Non usare NestedForm al suo interno
* Per form (datagrid) complessi è consigliata impostare una sola colonna e al suo interno aggiungere un componente Columns e creare la griglia desiderata con i vari input.

Nella sezione di riepilogo il componente al momento porta un problema di struttara dati mostrati i campi come vuoti. Per risolcvere questo problema è necessario aggiungere nell'ultima sezione utile del form un componente Hidden ed aggiungere in Calculated Value il seguente scripts, da modificare in base ai nomi scelti dei Datagrid

````
// uso un campo gancio della pagina visualizzata per farlo eseguire solo quando l'utente si trova in questa pagina
//Input gancio - qualsiasi altro input
var field = document.querySelector("input[name^='data[has_property]'");

//Controllo se il Datagrid ha almeno un elemento
if(data.property_list && data.property_list.length >0 && field){
  data.property_list.forEach((el,idx) =>{
    if(Array.isArray(el)){
        var newObj = data.property_list[idx];
          data.property_list[idx] = {...newObj};
    }
  })

}```
````
