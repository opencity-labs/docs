# Calcoli matematici

### Calcoli matematici e errori di approssimazione



Quando si usano componenti che richiedono calcoli matematici è consigliato aggiungere una funzione JS che corregge errori di approssimazione.

`function round(value, decimals) { return Number(Math.round(value+'e'+decimals)+'e-'+decimals); }`

Andare in modifica componente

&#x20;

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/90a15f99bcde5825bd9e82a825306c36/Schermata_2021-04-28_alle_11.01.20.png" alt=""><figcaption></figcaption></figure>

Sezione Data -> Calculated Values

&#x20;

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/105294033b037f9ade10e077da683a3e/Schermata_2021-04-28_alle_11.01.58.png" alt=""><figcaption></figcaption></figure>

Aggiungere la funzione che racchiuge il calcola da fare

&#x20;

<figure><img src="https://gitlab.com/opencontent/stanza-del-cittadino/core/-/wikis/uploads/5a27d8f0700ed594ed4597af2a39605b/Schermata_2021-04-28_alle_11.02.40.png" alt=""><figcaption></figcaption></figure>
