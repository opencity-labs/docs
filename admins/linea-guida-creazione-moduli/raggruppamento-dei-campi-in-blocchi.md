---
description: >-
  Per creare un modulo che sia aderente al modello del PNRR comuni 2022 sarà
  necessario raggruppare logicamente tutti i componenti del modulo in vari
  layout fieldset.
---

# Raggruppamento dei campi in blocchi

<figure><img src="../.gitbook/assets/image (3).png" alt=""><figcaption></figcaption></figure>

Una volta creati i vari layout fildset, inserirci tutti i componenti da raggruppare a livello logico, similmente a quanto fatto nel servizio di riferimento del design comuni [Iscrizione alla scuola dell'infanzia](https://italia.github.io/design-comuni-pagine-statiche/servizi/iscrizione-graduatoria-controllare-dati-personali.html).

<figure><img src="../.gitbook/assets/image (9).png" alt=""><figcaption><p>Come si vede il modulo in modifica</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (14).png" alt=""><figcaption><p>Come apparirà il blocco di dati</p></figcaption></figure>
