# Componente SELECT popolate via AP

Quando viene utilizzata il componente select popolato via api, nella pagina di riepilogo del modulo così come nella ricevuta pdf e nella pagina dell'operatore verrà visualizzato solo il valore del componente, ma non la sua label.

<figure><img src="../.gitbook/assets/image (6).png" alt=""><figcaption><p>Esempio</p></figcaption></figure>

Se il componente select viene configurato in questo modo:

<figure><img src="../.gitbook/assets/image (20).png" alt=""><figcaption></figcaption></figure>

Durante la compilazione del modulo vengono correttamente visualizzate tutte le opzioni disponibili:

<figure><img src="../.gitbook/assets/image (12).png" alt=""><figcaption></figcaption></figure>

nel momento in cui viene mostrata l'anteprima per il riepilogo non è più visibile la label dell'opzione selezionata, ma solo il suo valore. Nel caso in cui due valori non coincidano il risultato è il seguente:

<figure><img src="../.gitbook/assets/image (15).png" alt=""><figcaption></figcaption></figure>

#### Configurazione custom

La soluzione a questo problema è utilizzare l'opzione **custom** nella configurazione del componente select

<figure><img src="../.gitbook/assets/image (21).png" alt=""><figcaption></figcaption></figure>

e utilizzare una logica javascript personalizzata per il recupero delle opzioni della select

<figure><img src="../.gitbook/assets/image (23).png" alt=""><figcaption></figcaption></figure>

Questo esempio utilizza l'SDK Formio per interrogare le api dell'area personale, ma è anche possibile implementare una logica differente.

```javascript
async function get_data() {
    window.FormioHelper.authenticatedCall("categories")
        .then(function (promise) {
            const items = promise.map(elem => (
                {
                    id: elem.id,
                    name: elem.name
                })
            )
            instance.setItems(items)
        })
}
if (values.length === 0) {
    get_data()
}
```

Il risultato sarà il seguente:

<figure><img src="../.gitbook/assets/image (22).png" alt=""><figcaption></figcaption></figure>
