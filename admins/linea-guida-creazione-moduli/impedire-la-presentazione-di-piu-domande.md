# Impedire la presentazione di più domande

Un caso concreto: vogliamo bloccare l'invio di un bonus in ambito sociale, in modo che solo una famiglia possa fare richiesta.

Sarà indispensabile avere una base di dati o un'API da interrogare che ci da la chiave. Ipotiziamo che ci venga dato un file excel con i dati

| Codice famiglia | codice fiscale  |
| --------------- | --------------- |
| 1234            | SLVLNZxxxxxxxxx |

## Preparazione dei dati

Creiamo un JSON dal CSV, in modo che ogni riga del CSV diventi un oggetto di questo tipo:

```
{
"id": "SLVLNZxxxxxxxxx",
"famiglia": "1"
}
```

Si avvia un json-server per creare un'API su quei dati:

```
docker run -it [...] vimagick/json-server  /app/db.json --read-only 
```

Si testa con un client da CLI come [httpie](https://httpie.io/) che è possibile interrogare l'API:

```
http get https://www2.stanzadelcittadino.it/comune-di-xxx/api/anagrafe/SLVLNZxxxxxxxxx
```

Ottenendo un risultato di questo tipo:&#x20;

<figure><img src="../.gitbook/assets/image (24).png" alt=""><figcaption></figcaption></figure>

## Preparazione della form

Usiamo un campo di nome `unique_id` per far si che solo un componente del nucleo familiare possa inviare la domanda.

Si aggiunge al form un campo di tipo Text fied che si popolerà con il valore del family code quando si inserisce nella form il campo Codice Fiscale:

<figure><img src="../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/image (11).png" alt=""><figcaption></figcaption></figure>

Il valore del campo verrà calcolato in base a un piccolo script javascript da inserire nell'apposito campo _Data -> Calculated Value_

<figure><img src="../.gitbook/assets/image (16).png" alt=""><figcaption></figcaption></figure>



<figure><img src="../.gitbook/assets/image (27).png" alt=""><figcaption></figcaption></figure>

```
async function loadJSON(){
	response = await fetch(url, param)
	if (response.ok) {
		console.log('got successful from API')
		let responseData = await response.json()
		console.log("famiglia: " + responseData['famiglia'])
		if (!responseData.hasOwnProperty('famiglia')) {
			console.log('error: got empty response data:')
			console.log(responseData)
			console.log('error routine, setting not available value')
			instance.setValue('non disponibile')
		} else {
			console.log('got a non-null response Data')
			instance.setValue(responseData['famiglia'])
		}
	} else {
		console.log('error: got non 200 status code');
        console.log('error routine, setting not available value')
		instance.setValue('non disponibile')
	}
}

if (data.applicant.data.fiscal_code.data.fiscal_code.length == 16){
	let url = "/comune-di-xxxxx/api/anagrafe/" + data.applicant.data.fiscal_code.data.fiscal_code;
	let param = {
	  method: "get",
	  headers: {'Content-Type': 'application/json'}
	}
	loadJSON(url, param)
} else {
	console.log('not a valid value to perform api call')
	value = ""
}
```

Per validare il campo sarà necessaria una validazione in JS, per ottenere un controllo di questo tipo:

<figure><img src="../.gitbook/assets/image (13).png" alt=""><figcaption></figcaption></figure>

Validazione JS del campo

```
if (data.unique_id && data.unique_id !== "non disponibile") {
  valid = true
} else {
  valid = "Siamo spiacenti, il suo nucleo familiare non può beneficiare di questo servizio. Per maggiori informazioni contatti il Comune ai recapiti nel Box Informazioni presente in questa pagina."
}
```

## Controllo univocità

<figure><img src="../.gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>
