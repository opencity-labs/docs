---
description: >-
  Come aggiungere al modulo un componente che verrà mostrato solo in caso di
  compilazione da parte di un operatore.
---

# Componente solo per operatori

In un modulo potrebbe essere necessario poter rendere modificabile un campo solo lato operatore. Per poter popolare o modificare un campo solo se si è operatori, bisogna aggiungere la proprietà `operator_restricted` con valore `true` navigando nella tab `API` sotto la voce `Custom Properties`

<figure><img src="../.gitbook/assets/image (29).png" alt=""><figcaption><p>Esempio di un componente address visibile solo a un Operatore</p></figcaption></figure>

Aggiungere poi l'attributo `readonly` nel tab `Layout` sotto la voce `HTML attributes` con valore `true.`

<figure><img src="../.gitbook/assets/image (10).png" alt=""><figcaption><p>Configurazione del campo</p></figcaption></figure>

Aggiungere un condizione personalizzata per renderlo invisibile agli utenti. Aggiungere nella tab `Conditional` sotto `Advanced Conditions` questo codice `show = !instance.component.properties.hasOwnProperty('operator_restricted')` &#x20;

<figure><img src="../.gitbook/assets/image (31).png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

Con questa configurazione il cittadino non vedrà il campo, mentre resterà accessibile e modificabile agli operatori.
