# Table of contents

* [Gestione della piattaforma](README.md)
* [Installazione](installazione.md)
* [Primo avvio e configurazione](primo-avvio-e-configurazione.md)
* [Aggiornamenti](aggiornamenti.md)
* [Monitoraggio](monitoraggio.md)
* [Backup](backup.md)

## Release

* [Versione 2](release/versione-2/README.md)
  * [2.31.0](release/versione-2/2.31.0.md)
  * [2.30.0](release/versione-2/2.30.0.md)
  * [2.29.0](release/versione-2/2.29.0.md)
  * [2.28.0](release/versione-2/2.28.0.md)
  * [2.27.0](release/versione-2/2.27.0.md)
  * [2.26.0](release/versione-2/2.26.0.md)
  * [2.25.0](release/versione-2/2.25.0.md)
