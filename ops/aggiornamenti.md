---
description: >-
  Gli aggiornamenti sono pubblicati ad ogni release sul repository ufficiale
  della piattaforma e in forma di changelog.
---

# Aggiornamenti

Le modifiche elencate in modo dettagliato nel [CHANGELOG](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/CHANGELOG.md) presente nel repository principale sono sintetizzate con un linguaggio più amichevole in un [blog tecnico](https://changelog.stanzadelcittadino.it/), dove viene pubblicato un post per ogni release.&#x20;

La maggior parte delle operazioni di aggiornamento riguardano la struttura dei database e ogni sistema che ne fa uso si occupa in autonomia di effettuare i cambiamenti mediante un proprio sistema di _migrazioni_.

L'aggiornamento della piattaforma consiste per questo motivo nell'aggiornamento della versione dei microservizi, nelle pagine dedicate alle [release della piattaforma](broken-reference) sono disponibili le singole versioni rilasciate con l'elenco di tutte le versioni di tutti i microservizi.

In qualche caso è possibile che ci siano operazioni che non siamo riusciti a gestire in modo automatico, per questo motivo prima di applicare aggiornamenti consultare sempre il file [UPGRADE](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/UPGRADE.md) dove vengono elencate operazioni preliminari o da eseguire subito dopo l'upgrade.

Se non diversamente specificato è necessario eseguire l'aggiornamento in due step:

1. aggiornamento del microservizio app-manager, che esegue le migrazioni del core
2. aggiornamento di tutti gli altri microservizi
