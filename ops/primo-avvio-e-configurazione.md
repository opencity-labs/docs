# Primo avvio e configurazione

Una volta scelto il tipo di distribuzione, creato il file delle istanze e creati i database si potrà effettuare il deploy dei servizi che compongono la piattaforma.

Al primo avvio verranno svolte alcune operazioni in automatico:

* Esecuzione delle migrazioni su tutti i database delle istanze specificate nel file `instances.yml`
* Esecuzione delle migrazioni sul database del registry
* Creazione dei topics di kafka necessari
* Creazione degli stream e delle tabelle sul  servizio **ksqldb-server**

Una volta eseguite queste azioni automatiche dovranno essere finalizzate le configruazioni degli ambienti da parte dell'utente.

### Configurazione dell'utente admin per il registry

Entrare nel servizio **registry** ed eseguire il seguente comando:

```
/app/venv/bin/python3.9 manage.py createsuperuser
```

Verrà avviata una procedura guidata per la creazione dell'utente **admin** per il registry



### Installazione dei componenti base nel form server

L'installazione dei componenti base nel formserver può essere fatta in automatico tramite l'esecuzione del formserver-init (Per maggiori info guardare i file di distribuzione)

Queste operazioni possono essere eseguite anche manualmente, effettuato una post alle api del form server:

I componenti da creare sono:

[address](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/address.json), [birth-info](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/birth-info.json), [fiscal-code](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/fiscal-code.json), [full-name](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/full-name.json), [gender](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/gender.json), [iban](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/iban.json), [minor-personal-data](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/minor-personal-data.json), [personal-data-contacts](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/personal-data-contacts.json), [personal-data-light](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/personal-data-light.json), [personal-data-minimal](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/personal-data-minimal.json), [personal-data](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/personal-data.json), [residency](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/compose\_conf/formserver/init.d/residency.json)&#x20;

