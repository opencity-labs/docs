# Gestione della piattaforma

In questo documento sono descritte le attività operative per l'installazione e la manutenzione dei servizi della piattaforma. Il documento è orientato a sistemisti, platform engineers, SRE e tecnici addetti alla manutenzione della piattaforma in produzione.
