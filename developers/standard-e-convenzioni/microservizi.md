---
description: >-
  Tutti i microservizi che compongono la piattaforma devo rispettare i seguenti
  standard e regole generali
---

# Microservizi

Tutti i servizi devono rispettare i [12factor](https://12factor.net/), descritti in modo anche più dettagliato nell'articolo [An illustrated guide to 12 Factor Apps](https://www.redhat.com/architect/12-factor-app)

## Packaging <a href="#user-content-packaging" id="user-content-packaging"></a>

* `Dockerfile` e `docker-compose.yml` **devono** essere presenti nella root del repo (`.dockerignore` se serve) per facilitare l'avvio in locale dell'applicativo e farne saggiare l'utilizzo;
* se necessario usare un `docker-compose.dev.yml` per aggiungere tool utili per lavorare in locale durante lo sviluppo.

## Terminazione pulita <a href="#user-content-terminazione-pulita" id="user-content-terminazione-pulita"></a>

* il servizio **deve** gestire correttamente i segnali inviati da docker [SIGTERM e SIGKILL](https://www.ctl.io/developers/blog/post/gracefully-stopping-docker-containers/) per fare uno shutdown pulito e senza interrompere l'esecuzione in fasi critiche per la consistenza dei dati.

## Configurazione <a href="#user-content-configurazione" id="user-content-configurazione"></a>

* la configurazione \*_deve_ avvenire mediante variabili d'ambiente
* la configurazione può essere fatta anche tramite un file di environment (`.env`) o parametri da cli

## Healthcheck <a href="#user-content-healthcheck" id="user-content-healthcheck"></a>

* se http, **deve** essere presente un `/status` entrypoint che risponde 200 se va tutto bene, uno status code diverso in caso contrario
* se non http, l'healthcheck **può** essere un controllo su un file o su un processo
* l'health check, se disponibile, **deve** essere inserito nel Dockerfile

## Logging <a href="#user-content-logging" id="user-content-logging"></a>

* Il log **deve** supportare almeno tre livelli: `debug`, `info` e `error`.
* A `debug` deve essere comprensibile il flusso logico dell'applicativo, a `info`ci si aspetta di leggere 1 riga per ogni evento o chiamata ricevuta dall'applicativo,  a `error` invece non si deve vedere nulla se non ci sono errori: per capirsi, a ERROR, il _rate_ dei log che si alza dovrebbe essere indice che ci sono problemi per i quali è importante attrarre l'attenzione degli amministratori.
* Se sono previste chiamate http, **deve** avere un log stile webserver, con indicazione dell'IP chiamante (con supporto di `X-Forwarded-` headers, timestamp, path servito, etc... fate riferimento allo standard ncsa o apache log format, in tutti i framework ci sono librerie che li producono _gratis_.
* Non mi schiare log http e log multiriga (stacktrace o similari): per esempio inviare lo stacktrace a `stderr` e riservare `stdout` ai log http
* Se possibile implementare log in formato json, ma solo se si può produrre solo del JSON, mischiare testo semplice e JSON non è di grande utilità.

## Monitoring errors <a href="#user-content-monitoring-errors" id="user-content-monitoring-errors"></a>

* sentry support
* configurazione di sentry sul repo gitlab

## Monitoring metrics <a href="#user-content-monitoring-metrics" id="user-content-monitoring-metrics"></a>

* se http, **deve** esistere l'entrypoint `/metrics` che espone metriche in formato prometheus.
* un servizio **può** mostrare metriche sulle chiamate (status code, timing), anche se solitamente le possiamo prendere altrove (traefik), - un servizio **deve** mostrare metriche specifiche dell'app, in particolare delle condizioni di errore (counters) e dei tempi di risposta di eventuali servizi esterni richiamati da questo (histograms).

## Cron <a href="#user-content-cron" id="user-content-cron"></a>

* sui sistemi di deploy e orchestrazione che usiamo ci sono sempre dei modi di eseguire task stile cron, inutile reimplementare quella logica dentro la propria App.
* è indispensabile invece rendere il proprio software idempotente: deve essere rieseguibile più volte sugli stessi dati senza fare danno.
* il modo più semplice per implementare una chiamata a tempo è implementare un comando da cli che possa essere eseguito con la stessa immagine con cui gira l'applicativo.

## Parametri e file di configurazione <a href="#user-content-parametri-e-file-di-configurazione" id="user-content-parametri-e-file-di-configurazione"></a>

* in docker c'e' sempre un mapping tra esterno e interno, inutile parametrizzare questi aspetti in modo particolarmente flessibile.

## CI <a href="#user-content-ci" id="user-content-ci"></a>

* linting del codice e enforcemente dei coding styles

## Public Code <a href="#user-content-public-code" id="user-content-public-code"></a>

* pubblicazione su Developers Italia
