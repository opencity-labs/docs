---
description: Riguardo a coding, contribuzioni, eventi, API
---

# Standard della piattaforma

### Event fields

#### Producers

Nella piattaforma si fa ampio uso del Pattern [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html) è quindi fondamentale che il formato degli eventi sia stabile e documentato.

Per ogni evento ci aspettiamo almeno i seguenti campi:

| Campo              | Valore                                                    | Formato                 | Annotazioni                                                                                                                                                                                                                                      |
| ------------------ | --------------------------------------------------------- | ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `id`               | identificativo unico dell'entità oggetto dell'evento      | UUID                    | Es: in un pagamento è l'ID del pagamento, in una pratica è l'application\_id etc...                                                                                                                                                              |
| `event_id`         | identificativo unico dell'evento                          | UUID                    | Non deve esserci per nessun motivo due volte lo stesso ID                                                                                                                                                                                        |
| `event_version`    | versione dell'evento                                      | Integer or Float        | Se il formato è solo un intero (Es: 1, 2, etc..) cambia ad ogni singolo cambiamento. Se si usa un float `1.0`, `1.1`, etc... il formato deve essere retrocompatibile se cambia la parte decimale, non retrocompatible se cambia la parte intera. |
| `event_created_at` | data dell'evento                                          | ISO8601                 | Ricordarsi di usare il formato della timezone Europe/Rome (Es: `2022-06-22T15:11:20+02:00`)                                                                                                                                                      |
| `app_id`           | Nome e versione dell'applicativo che ha generato l'evento | `$application:$version` | Utile per motivi di debug (Es: payment-dispatcher:1.0.15).                                                                                                                                                                                       |

Attenzione: se si genera un evento, a partire da un evento esistente, si sta creando una nuova entità che avrà i propri valori `event_id`, `event_created_at`, `event_version`, `app_id`: non si deve mai ricopiare questi valori dall'evento originario.

#### Consumers

I consumatori devono essere sempre attenti alla versione di eventi che supportano: la versione deve essere esplicitamente supportata ed eventi di una versione non supportata devono essere scartati

Nei log è possibile dare evidenza del fatto che si è incontrato un evento non supportato, ma non va loggato come errore è sufficiente dare l'informazione a livello DEBUG.

## API Standards

Facciamo in generale riferimento alle [linee guida di Zalando](https://opensource.zalando.com/restful-api-guidelines/):&#x20;

```
POST    /tenants
GET     /tenants/{tenant_id}
PUT     /tenants/{tenant_id}
PATCH   /tenants/{tenant_id}
DELETE  /tenants/{tenant_id}
```

#### GET

* Le collezioni devono essere facilmente navigabili, per questo abbiamo adottato il seguente standard:

```json
{
    "meta": {
        "page": {
            "offset": 5000,
            "limit": 20,
            "sort": "creationTime"
        },
        "total": 46561
    },
    "links": {
        "self": "http..../items?offset=5000&limit=20&sort=creationTime",
        "prev": "http..../items?offset=4980&limit=20&sort=creationTime",
        "next": "http..../items?offset=5020&limit=20&sort=creationTime"
    },
    "data": [
        { "id": "..." },
        { "id": "..." },
        { "id": "..." }
    ]
}
```

#### Pagination

* Le GET sulle risorse devono supportare i query parameters convenzionali:
* `sort`
* `offset`
* `limit`

#### Pagination/2

* Su alcuni campi è utile implementare anche l'opzione di paginazione cursor-based:

`GET /payments?created_since=$timeStamp`

#### error handling

```json
{ 
    "errors": [
        {
            "type": "/errors/incorrect-user-pass",
            "title": "Incorrect username or password.",
            "status": 401,                                         (optional)
            "detail": "Authentication failed due to incorrect username or password.",
            "instance": "/login/log/abc123"                        (optional)
        },
        ...
    ]
}
```

Fare riferimento alla [RFC 7807](https://tools.ietf.org/html/rfc7807)
