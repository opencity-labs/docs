# Usecase: Il Documento originato dalle pratiche dai servizi digitali

La generazione di documenti varia a seconda del tipo di pratica che deve essere registrata o gestita. Ci sono diverse tipologie di pratica, ognuna delle quali corrisponde a uno stato specifico della procedura:

1. **Creazione di una pratica da parte o per conto di un cittadino (application-request)**: questo si riferisce alla creazione iniziale di una pratica da parte di un cittadino o da parte di qualcuno agendo per conto del cittadino.
2. **Invio di una richiesta di integrazioni da parte di un operatore (integration-request)**: questo si verifica quando un operatore richiede ulteriori informazioni o documenti relativi a una pratica.
3. **Invio di un'integrazione da parte del cittadino (integration-response)**: In questo caso, il cittadino risponde alle richieste di integrazione inviando ulteriori documenti o informazioni.
4. **Approvazione/rifiuto di una pratica da parte di un operatore (application-outcome, application-revocation)**: questo rappresenta l'atto di approvare o rifiutare una pratica da parte di un operatore.&#x20;
5. **Ritiro di una pratica da parte di un cittadino (application-withdraw)**: un cittadino può ritirare una pratica in qualsiasi momento.

Queste tipologie rappresentano diversi stati o fasi attraverso cui una pratica può passare durante il suo ciclo di vita. Poiché una pratica può attraversare più di uno di questi stati, ciò significa che **saranno generati diversi documenti associati alla stessa pratica**, ognuno dei quali corrisponderà a una fase specifica del processo.
