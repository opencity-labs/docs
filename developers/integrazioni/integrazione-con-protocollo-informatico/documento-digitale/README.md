---
description: Descrizione in dettaglio di un documento generato dalla piattaforma
---

# Documento digitale

## Contesto

Secondo l'AGID, il documento informatico è la “_rappresentazione informatica di atti, fatti o dati giuridicamente rilevanti" in contrapposizione al documento analogico ("rappresentazione non informatica di atti, fatti o dati giuridicamente rilevanti")"._

La piattaforma genera documenti informatici a partire da diversi tipi di servizi offerti.

Il documento informatico ci permette di avere un'unica struttura dati a partire da diverse tipologie di servizi.  Questo ci dà il vantaggio di dover gestire un’unica entità durante il processo di protocollazione.&#x20;



## Struttura&#x20;

Il documento generato dall’area personale del cittadino è stato progettato seguendo [le linee guida suggerite dall’AGID](https://www.agid.gov.it/it/piattaforme/sistema-gestione-procedimenti-amministrativi/documento-informatico). Esso è composto dai seguenti elementi:&#x20;

* **Documento principale**: descrive in dettaglio il servizio o l'oggetto a cui il documento si riferisce.
* **Allegati del documento principale**: rappresentano l'insieme dei file che accompagnano la documento principale, fornendo ulteriori dettagli, dati o documentazione di supporto.
* **metadati**: includono informazioni utili per la comprensione del contesto del documento informatico. Ci forniscono dettagli sulle informazioni di base, come la data di creazione, il mittente, il destinatario, e altri dettagli pertinenti. Alcuni di essi non possono mancare(vedi sezione seguente).

##

## Specifiche funzionali del documento

Nella nostra piattaforma il documento è implementato in forma di evento Kafka, in formato JSON, descritto dal seguente JSON SCHEMA.

Per una migliore visualizzazione si consiglia di usare un [viewer](https://codebeautify.org/jsonviewer/c6a219) online.

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema#",
  "$id": "https://schema.opencontent.io/document.schema#",
  "type": "object",
  "title": "Documento",
  "definitions": {
    "registration_info": {
      "type": "object",
      "title": "Informazioni sul protocollo del documento",
      "properties": {
        "transmission_type": {
          "title": "Tipo di trasmissione",
          "type": "string",
          "enum": [
            "inbound",
            "outbound",
            "internal"
          ],
          "examples": [
            "inbound"
          ]
        },
        "date": {
          "title": "Data protocollo",
          "type": "string",
          "format": "date-time",
          "examples": [
            "2022-08-24T11:59:57+02:00"
          ]
        },
        "document_number": {
          "title": "Numero di protocollo",
          "description": "Numero di protocollo assegnato dal sistema di protocollazione",
          "type": "string",
          "maxLength": 255,
          "examples": [
            "Prot-123-456-789",
            "123"
          ]
        },
        "registry_code": {
          "title": "Identificativo interno del registro",
          "description": "Eventuale numero identificativo assegnato o codice di risposta restituito dal sistema di protocollazione. Dipende dall'implementazione del sistema di protocollo",
          "type": [
            "string",
            "null"
          ],
          "examples": [
            "OK",
            "123"
          ]
        }
      },
      "required": [
        "transmission_type",
        "date",
        "document_number",
        "registry_code"
      ]
    },
    "folder_info": {
      "type": "object",
      "title": "Informazioni sul fascicolo",
      "properties": {
        "title": {
          "title": "Oggetto del Fascicolo",
          "type": [
            "string",
            "null"
          ],
          "maxLength": 255
        },
        "id": {
          "title": "Numero di Fascicolo",
          "type": [
            "string",
            "null"
          ],
          "maxLength": 255
        }
      },
      "required": [
        "title",
        "id"
      ]
    },
    "file": {
      "title": "File",
      "type": "object",
      "properties": {
        "name": {
          "title": "Titolo del file",
          "type": "string"
        },
        "description": {
          "title": "Descrizione del file",
          "type": [
            "string",
            "null"
          ]
        },
        "mime_type": {
          "title": "Mime type del file",
          "type": "string"
        },
        "url": {
          "title": "Download url della risorsa",
          "type": "string",
          "format": "uri"
        },
        "md5": {
          "title": "Hash md5 del binario",
          "type": "string"
        },
        "filename": {
          "title": "Nome completo del file con estensione",
          "type": "string"
        },
        "internal_url": {
          "title": "Eventuale url interno della risorsa a disposizione dei sistemi dell'ente che sarà utilizzato per servire il binario se questo non viene salvato nello storage della piattaforma",
          "type": ["string", "null"],
          "format": "uri"
        }
      },
      "required": [
        "name",
        "filename",
        "description",
        "mime_type",
        "url",
        "md5"
      ]
    },
    "image": {
      "type": "object",
      "title": "Risorsa immagine",
      "properties": {
        "name": {
          "title": "Titolo dell'immagine",
          "type": "string"
        },
        "filename": {
          "title": "Nome dell'immagine",
          "type": "string"
        },
        "description": {
          "title": "Descrizione dell'immagine",
          "type": "string"
        },
        "license": {
          "title": "Licenza di utilizzo",
          "type": "string",
          "enum": [
            "?",
            "??",
            "???"
          ]
        },
        "type": {
          "title": "Tipologia dell'immagine o mime-type",
          "type": "string"
        },
        "url": {
          "title": "Url della risorsa",
          "type": "string",
          "format": "uri"
        }
      },
      "required": [
        "name",
        "filename",
        "license",
        "type",
        "url",
        "description"
      ]
    },
    "author": {
      "type": "object",
      "title": "Autore",
      "properties": {
        "type": {
          "type": "string",
          "enum": [
            "human",
            "legal"
          ]
        },
        "tax_identification_number": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "family_name": {
          "type": [
            "string",
            "null"
          ]
        },
        "street_name": {
          "type": [
            "string",
            "null"
          ]
        },
        "building_number": {
          "type": [
            "string",
            "null"
          ]
        },
        "postal_code": {
          "type": [
            "string",
            "null"
          ]
        },
        "town_name": {
          "type": [
            "string",
            "null"
          ]
        },
        "country_subdivision": {
          "type": [
            "string",
            "null"
          ]
        },
        "country": {
          "type": [
            "string",
            "null"
          ]
        },
        "email": {
          "type": "string",
          "format": "email"
        }
      },
      "required": [
        "type",
        "tax_identification_number",
        "name",
        "family_name",
        "street_name",
        "building_number",
        "postal_code",
        "town_name",
        "country_subdivision",
        "country",
        "email",
        "role"
      ]
    }
  },
  "properties": {
    "title": {
      "type": "string",
      "maxLength": 255,
      "title": "Titolo del documento",
      "description": "Nome del documento. Il nome del Documento deve essere facilmente comprensibile dai cittadini. Vincoli: massimo 160 caratteri spazi inclusi",
      "examples": [
        "Istanza di certificato anagrafico"
      ]
    },
    "id": {
      "type": "string",
      "format": "uuid",
      "title": "Identificativo interno del documento",
      "description": "Identificatore uuid ad uso interno",
      "examples": [
        "4a68415b-e1a8-4f3a-a229-3f6678fd81d1"
      ]
    },
    "version": {
      "type": "integer",
      "title": "Versione del documento",
      "description": "?????",
      "default": 1,
      "examples": [
        1
      ]
    },
    "external_id": {
      "type": [
        "string",
        "null"
      ],
      "title": "Identificativo del documento ad uso dell'ente",
      "description": "Un numero identificativo del documento (es DOI, ISBN)",
      "examples": [
        "Cert567-2023",
        "4a68415b-e1a8-4f3a-a229-3f6678fd81d1"
      ]
    },
    "registration_data": {
      "title": "Protocollo",
      "anyOf": [
        {
          "title": "Informazioni sul protocollo del documento",
          "$ref": "#/definitions/registration_info"
        },
        {
          "title": "Nessuna informazione sul protocollo",
          "type": "null"
        }
      ]
    },
    "folder": {
      "title": "Fascicolo",
      "anyOf": [
        {
          "title": "Informazioni sul fascicolo",
          "$ref": "#/definitions/folder_info"
        },
        {
          "title": "Nessuna informazione sul fascicolo",
          "type": "null"
        }
      ]
    },
    "type": {
      "title": "Tipo di documento",
      "description": "Tipologia del documento: viene utilizzata una tassonomia custom che va intesa come sotto tassonomia di Istanza https://schema.gov.it/lodview/controlled-vocabulary/classifications-for-documents/government-documents-types/8",
      "type": "string",
      "enum": [
        "instance.request",
        "instance.withdraw",
        "instance.integration-request",
        "instance.integration-response",
        "instance.revocation",
        "instance.outcome"
      ]
    },
    "remote_id": {
      "title": "Pratica associata",
      "description": "Identificativo della pratica associata",
      "type": ["string", "null"],
      "format": "uuid",
      "examples": [
        "842b37e7-d835-49f0-92e2-a393ecda2e53"
      ]
    },
    "remote_collection": {
      "title": "Servizio collegato",
      "anyOf": [
        {
          "title": "Servizio collegato",
          "description": "Se il documento non è collegato a una pratica è possibile indicare il servizio che ha generato il documento",
          "type": "object",
          "properties": {
            "id": {
              "type": "string",
              "format": "uuid",
              "examples": [
                "842b37e7-12a4-49f0-92e2-a393ecda2e53"
              ]
            },
            "type": {
              "type": "string",
              "enum": [
                "service",
                "service-group",
                "subscription",
                "calendar"
              ],
              "examples": [
                "service"
              ]
            }
          },
          "required": [
            "id",
            "type"
          ]
        },
        {
          "title": "Nessuna servizio collegato",
          "type": "null"
        }
      ]
    },
    "topics": {
      "title": "Argomenti",
      "description": "Argomenti collegabili",
      "type": "array",
      "minItems": 0,
      "items": {
        "title": "Argomento",
        "type": "string",
        "minItems": 0
      }
    },
    "short_description": {
      "title": "Descrizione breve",
      "description": "Descrizione sintetica del documento (max 255 caratteri) utilizzando un linguaggio semplice che possa aiutare qualsiasi utente a identificare con chiarezza il documento. Non utilizzare un linguaggio ricco di riferimenti normativi",
      "type": "string",
      "maxLength": 255
    },
    "description": {
      "title": "Descrizione",
      "description": "L'oggetto del documento, utilizzando un linguaggio semplice che possa aiutare qualsiasi utente a identificare con chierazza il documento. Non utilizzare un linguaggio ricco di riferimenti normativi.",
      "type": [
        "string",
        "null"
      ]
    },
    "main_document": {
      "title": "URL documento",
      "$ref": "#/definitions/file"
    },
    "image_gallery": {
      "title": "Galleria di immagini",
      "type": "array",
      "items": {
        "$ref": "#/definitions/image"
      },
      "minItems": 0
    },
    "has_organization": {
      "title": "Ufficio responsabile del documento",
      "anyOf": [
        {
          "title": "Link alla scheda dell'ufficio responsabile del documento",
          "type": "string",
          "format": "uri",
          "examples": [
            "https://example.com/offices/1"
          ]
        },
        {
          "title": "Nessuna informazione sull'ufficio",
          "type": "null"
        }
      ]
    },
    "attachments": {
      "title": "Allegati",
      "description": "Altre risorse oltre alla principale",
      "type": "array",
      "minItems": 0,
      "items": {
        "$ref": "#/definitions/file"
      }
    },
    "distribution_license_id": {
      "title": "Licenza di distribuzione",
      "type": [
        "string",
        "null"
      ]
    },
    "related_public_services": {
      "title": "Servizi collegati",
      "description": "Servizi che usano questo documento come input",
      "type": "array",
      "items": {
        "type": "object",
        "title": "Servizio collegato",
        "properties": {
          "name": {
            "id": "Id del servizio",
            "type": "string"
          },
          "type": {
            "title": "Tipologia del servizio",
            "type": "string"
          },
          "url": {
            "title": "Descrizione del file",
            "type": "string",
            "format": "uri"
          }
        },
        "required": [
          "id",
          "type",
          "url"
        ]
      },
      "minItems": 0
    },
    "valid_from": {
      "title": "Data inizio validità",
      "description": "Data da cui il documento è valido",
      "type": [
        "string",
        "null"
      ],
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    },
    "valid_to": {
      "title": "Data fine validità",
      "description": "Data da cui il documento non è più valido",
      "type": [
        "string",
        "null"
      ],
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    },
    "removed_at": {
      "title": "Data ultima disponibilità",
      "description": "Data fino alla quale il documento sarà disponibile online",
      "type": [
        "string",
        "null"
      ],
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    },
    "expire_at": {
      "title": "Data scadenza",
      "description": "Solitamente uguale a fine validità, è la data entro la quale il documento deve essere eventualmente rinnovato",
      "type": [
        "string",
        "null"
      ],
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    },
    "more_info": {
      "title": "Ulteriori informazioni",
      "description": "Ulteriori informazioni sul documento",
      "type": [
        "string",
        "null"
      ]
    },
    "normative_requirements": {
      "title": "Riferimenti normativi",
      "description": "Lista di link con riferimenti normativi utili per il documento",
      "type": "array",
      "minItems": 0,
      "items": {
        "type": "string",
        "format": "uri"
      }
    },
    "related_documents": {
      "title": "Documenti collegati",
      "description": "Lista di documenti allegati: link a quelli strutturati a loro volta come documenti",
      "type": "array",
      "minItems": 0,
      "items": {
        "type": "string",
        "format": "uri"
      }
    },
    "life_events": {
      "title": "Life Events",
      "description": "Life Events collegabili",
      "type": "array",
      "minItems": 0,
      "items": {
        "type": "string"
      }
    },
    "business_events": {
      "title": "Business Events",
      "description": "Business Events collegabili",
      "type": "array",
      "minItems": 0,
      "items": {
        "type": "string"
      }
    },
    "allowed_readers": {
      "title": "Lista di identificatori di persone che possono accedere in lettura al documento",
      "description": "??? Vedi onotologia...",
      "type": "array",
      "minItems": 0,
      "items": {
        "type": "string"
      }
    },
    "tenant_id": {
      "title": "Ente",
      "type": "string",
      "format": "uuid",
      "description": "Uuid del tenant",
      "examples": [
        "842b37e7-d835-49f0-92e2-a393ecda2e53"
      ]
    },
    "owner_id": {
      "title": "Proprietario",
      "description": "Uuid del proprietario del documento",
      "type": "string",
      "format": "uuid"
    },
    "document_url": {
      "title": "Url pubblico del documento",
      "type": [
        "string",
        "null"
      ],
      "format": "uri"
    },
    "created_at": {
      "title": "Data di creazione",
      "type": "string",
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    },
    "updated_at": {
      "title": "Data ultima modifica",
      "type": "string",
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    },
    "author": {
      "title": "Autore/i",
      "description": "Persone che hanno redatto il documento",
      "type": "array",
      "minItems": 0,
      "items": {
        "$ref": "#/definitions/author"
      }
    },
    "source_type": {
      "title": "Tipologia del soggetto che ha generato il documento",
      "description": "In base a questo che si identifica la transmission type",
      "type": "string",
      "enum": [
        "tenant",
        "user"
      ]
    },
    "recipient_type": {
      "title": "Tipologia di destinatario del documento",
      "description": "In base a questo che si identifica la transmission type",
      "type": "string",
      "enum": [
        "tenant",
        "user"
      ]
    },
    "last_seen": {
      "title": "Data ultima visualizzazione da parte del destinatario",
      "type": [
        "string",
        "null"
      ],
      "format": "date-time",
      "examples": [
        "2022-08-24T11:59:57+02:00"
      ]
    }
  },
  "required": [
    "title",
    "id",
    "version",
    "type",
    "short_description",
    "main_document",
    "tenant_id",
    "owner_id",
    "created_at",
    "updated_at",
    "source_type",
    "recipient_type"
  ]
}

```

### Esempio di documento

```json
{
	"title": "FINE LAVORI PARZIALE P.ED. 1111",
	"id": "123456qwert-87fd-qwe-1232w-12345qwert",
	"version": 1,
	"folder": {
		"title": "Dichiarazione Ultimazione Lavori-MARIO ROSSI XXXXXX72D23L378L",
		"id": ""
	},
	"type": "integration-request",
	"remote_id": "111111aa-e17e-11aa-22ww-d456",
	"remote_collection": {
		"id": "",
		"type": "service"
	},
	"topics": null,
	"short_description": "FINE LAVORI PARZIALE P.ED. 1111",
	"description": "FINE LAVORI PARZIALE P.ED. 1111",
	"main_document": {
		"name": "richiesta-integrazione-12345678-1234-1q2w-1q2w-1q2w3e4r-202310190357.pdf.p7m",
		"description": "Richiesta integrazione: 12345678-1234-1q2w-1q2w-1q2w3e4r 2023-10-19 03:57",
		"mime_type": "application/octet-stream",
		"url": "https://www2.stanzadelcittadino.it/comune-di-mezzocorona/api/applications/111111aa-e17e-11aa-22ww-d456/attachments/12345678-1234-1q2w-1q2w-1q2w3e4r?version=1",
		"md5": "",
		"filename": "1234345.pdf.p7m"
	},
	"image_gallery": null,
	"attachments": null,
	"related_public_services": null,
	"normative_requirements": null,
	"related_documents": null,
	"life_events": null,
	"business_events": null,
	"allowed_readers": null,
	"tenant_id": "57c26a77-3988-48d0-81f7-751294d89aa8",
	"owner_id": "76fa7c01-a13d-4699-94a0-72d06d76c892",
	"created_at": "2023-10-19T13:57:54+00:00",
	"updated_at": "",
	"author": [
		{
			"type": "human",
			"tax_identification_number": "XXXXXX72D23L378L",
			"name": "MARIO",
			"family_name": "ROSSI",
			"email": "fake@mail.it",
			"role": "receiver"
		}
	],
	"source_type": "tenant",
	"recipient_type": "user"
}
```

