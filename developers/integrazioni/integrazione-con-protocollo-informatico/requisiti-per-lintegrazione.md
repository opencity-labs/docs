# Requisiti per l'integrazione

L’area personale del cittadino effettua diverse interazioni con il sistema di protocollo, ma tutte seguono una logica comune. Le interazioni avvengono in seguito ad alcune azioni da parte cittadini e degli operatori:

1. invio di una pratica da parte del cittadino
2. invio di una richiesta di integrazioni da parte di un operatore
3. invio di una integrazione da parte del cittadino
4. approvazione/rifiuto di una pratica

Altre azioni possibili, mediante configurazioni avanzate del servizio:

5. creazione di una pratica da parte di un operatore per conto di un cittadino
6. ritiro di una pratica da parte del cittadino
7. annullamento di una pratica da parte di un operatore

### Metadati utilizzati

Per ognuna di queste operazioni viene prodotto un documento principale dotato di eventuali allegati.&#x20;

Per ogni documento ci aspettiamo di poter sempre specificare:

* Un oggetto
* Un mittente (il soggetto che ha formato il documento)
* Un destinatario (solitamente l’ufficio che deve prendere in carico la richiesta)
* Data di registrazione della pratica
* Tipo di trasmissione (in entrata, in uscita, interno)
* Classificazione (solitamente sotto forma di indice di classificazione nell’albero del titolario, es 6.3.4)

Per ogni documento protocollato ci aspettiamo di ricevere i seguenti dati:

* Il Numero di protocollo (un identificativo unico e persistente)
* Data di registrazione nel protocollo
* (opzionale) Una impronta digitale del documento protocollato

Tutti i documenti di una pratica sono raccolti in un fascicolo per il quale ci aspettiamo di poter specificare

* L’oggetto
* (opzionale) La classificazione

### Flusso di interazione tra Area Personale e sistema di protocollo

Per ogni integrazione sono necessarie solitamente le seguenti chiamate API o webservice:

1. Ricerca di un documento per Numero di protocollo
2. Invio di un Documento principale
3. Invio di allegati al documento principale
4. Protocollazione di un documento e dei suoi allegati: se disponibile, preferiamo avere una chiamata atomica, che protocolla il documento principale e i suoi allegati.
5. Ricerca di un fascicolo per Numero di fascicolo
6. Creazione di un fascicolo
7. Aggiunta di un documento ad un fascicolo esistente

### Processo di integrazione

1. Condivisione documentazione:

* Documentazione delle API o dei WebService
* ambiente di test (endpoint, credenziali di accesso)
* Esempio di chiamate necessarie per fare una protocollazione di un documento e dei suoi allegati

1. Test delle chiamate al sistema di protocollo effettuate in modo manuale
2. Validazione della protocollazione manuale da parte dell’Ente
3. Implementazione e rilascio dell’integrazione in ambiente di Quality Assurance
4. Validazione su un servizio di test
5. Rilascio in ambiente di produzione
6. (opzionale) Validazione su un servizio di produzione e annullamento della protocollazione effettuata

L’ultimo test effettuato in ambiente di produzione non è strettamente necessario, ma nella nostra esperienza può rivelare differenze nella configurazione del sistema di protocollo che possono diventare bloccanti.
