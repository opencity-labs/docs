# Protocol Proxy: Specifiche Implementative

### Requisiti Generali - controllare pagina standard e convenzioni&#x20;

* Ongni microservizio deve rispettare gli[standard della piattaforma](https://docs.opencityitalia.it/v/developers/standard-e-convenzioni/standard-della-piattaforma)&#x20;
* Ogni proxy deve soddisfare i requisiti suggeriti dal [12 factors manifest](https://12factor.net/it/)
* Esposizione di un endpoint(/status) per l'healtcheck: esso deve essere restituire 200 se il sistema è "healty" e 503 in caso si presenti uno dei seguenti problemi: connessione con kafka non disponibile.
* Esposizione delle metriche compatibili(endpoint /metrics) con Prometheus utilizzando [la convenzione da loro suggerita](https://prometheus.io/docs/practices/naming/): si vogliono misurare due metriche di tipo counter, di cui una per le protocollazioni riuscite con successo e una che misuri quelle fallite. Le labels delle metriche devono indicare le seguente informazioni: "env" per indicare l'ambiente di sviluppo(LOCAL, DEV, QA, PROD) e "tenant"(in forma testuale in base al relativo tenant id).&#x20;
* Terminazione pulita del servizio(timeout di 15 secondi)&#x20;
* Esposizione di un endpoint(/docs) per la consultazione della documentazione delle API esposte dal servizio in base alla standard OpenAPI
* Integrazione con [Sentry](https://https/.sentry.io) per l'error reporting
* Il sistema di logging deve rispettare il formato Apache descrivere nei tre livelli debug, error e info. Quando posisible, loggare il document ID  relativo all'evento che si processa.&#x20;
* il readme deve seguire le [linee guida della pubblicazione del software open source](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/attachments/allegato-a-guida-alla-pubblicazione-open-source-di-software-realizzato-per-la-pa.html#file-readme)&#x20;
* il nome del progetto e del servizio devono essere nella seguente forma : "Protocol Proxy \<Nome del Protocollo Specifico>"

### Requisiti normativi

* Avere una specifica in formato OpenAPI v3
* Rispettare la [Linee Guida di interoperabilità](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs): in particolare per quanto riguarda&#x20;
  * [il formato dei dati](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs/it/bozza/doc/04\_Raccomandazioni%20di%20implementazione/04\_raccomandazioni-tecniche-generali/02\_formato-dei-dati.html) (4.2)
  * [raccomandazioni sul naming](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs/it/bozza/doc/04\_Raccomandazioni%20di%20implementazione/04\_raccomandazioni-tecniche-generali/03\_progettazione-e-naming.html) (4.3) optando per la scelta di `snake_case` per i nomi degli attributi
  * logging (4.4)
  * risultare valido nel [validatore ufficiale OA3](https://github.com/italia/api-oas-checker)

### Documentazione API - Swagger&#x20;

[Protocol Proxy example - swagger UI](https://api.qa.stanzadelcittadino.it/registry-proxy/fake/v1/docs/index.html)





### Specifiche funzionali

* Il servizio deve essere multi tentant e multi servizio: deve quindi essere in grado di gestire le confiugurazioni per più tenant a cui possono essere attribuiti più servizi con il corrente sistema di protocollazione.&#x20;
* Ogni richiesta sia essa appartenente alla fase 1 o alla fase 2 descritte nella sezione "[workflow sistema di protocollazione](../integrazioni-con-il-flusso-delle-pratiche/webhooks.md)" devono  essere trattate in maniera atomica.
* Lo storage deve essere compatibile con Aws s3, Azure e local file system.



### Configurazione del servizio(variabili d'ambiente)

Il servizio deve essere configurabile attraverso le seguenti variabili d'ambiente.&#x20;

Se le varibili senza valore di default non vengono settate, il servizio deve notificare la mancata impostazione attraverso un log di livello error e terminare in maniera pulita la propria esecuzione.&#x20;

| Nome                             | Default                | Descrizione                                                                                                                                                 |
| -------------------------------- | ---------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ENVIRONMENT                      | local                  | Indica l'ambiente di sviluppo (locale, dev, prod, ecc.) utilizzato da Sentry.                                                                               |
| DEBUG                            | true                   | ...                                                                                                                                                         |
| SENTRY\_ENABLED                  | false                  | ...                                                                                                                                                         |
| SENTRY\_DSN                      | nessun valore          | Endpoint per il monitoraggio di Sentry.                                                                                                                     |
| KAFKA\_SERVER                    | kafka:9092             | <p>Lista di lunghezza variabile i cui elementi devono essere separati da virgola.<br>lista degli Indirizzi dei broker Kafka per connettersi al cluster.</p> |
| KAFKA\_CONSUMER\_GROUP           | \<nome\_del\_servizio> | Consumer group per Kafka.                                                                                                                                   |
| KAFKA\_CONSUMER\_TOPIC           | documents              | Identifica il topic da cui consumare gli eventi Kafka.                                                                                                      |
| KAFKA\_PRODUCER\_TOPIC           | documents              | Identifica il topic a cui inviare gli eventi Kafka.                                                                                                         |
| KAFKA\_RETRY\_TOPIC              | nessun valore          | topic in cui produrre gli eventi consumati dal meccanismo di retry                                                                                          |
| SERVER\_ADDRESS\_PORT            | 0.0.0.0:8080           | Indica l'indirizzo e la porta utilizzati per l'healthcheck.                                                                                                 |
| CACHE\_EXPIRATION                | 5m                     | ...                                                                                                                                                         |
| CACHE\_EVICTION                  | 10m                    | ...                                                                                                                                                         |
| STORAGE\_TYPE                    | local                  | Tipo di storage dei pagamenti: s3, azure, local                                                                                                             |
| STORAGE\_ENDPOINT                | nessun valore          | Indirizzo di accesso allo storage.                                                                                                                          |
| STORAGE\_ACCESS\_S3\_KEY         | nessun valore          | Chiave di accesso allo storage.                                                                                                                             |
| STORAGE\_KEY\_S3\_ACCESS\_SECRET | nessun valore          | Chiave segreta di accesso allo storage.                                                                                                                     |
| STORAGE\_S3\_REGION              | nessun valore          | Location del cloud storage                                                                                                                                  |
| STORAGE\_BUCKET                  | nessun valore          | Nome dello storage                                                                                                                                          |
| STORAGE\_BASE\_PATH              | "/data/"               | Basepath dello storage                                                                                                                                      |
| STORAGE\_AZURE\_ACCOUNT          | nessun valore          | Chiave dello storage AZURE                                                                                                                                  |
| STORAGE\_AZURE\_KEY              | nessun valore          | Password dello storage AZURE                                                                                                                                |
| STORAGE\_LOCAL\_PATH             | /data/                 |                                                                                                                                                             |
| SDC\_AUTH\_TOKEN\_USER           | nessun valore          | utente autenticazione per recuperare il token dalla piattaforma                                                                                             |
| SDC\_AUTH\_TOKEN\_PASSWORD       | nessun valore          | password autenticazione per recuperare il token dalla piattaforma                                                                                           |

###

###

### Test

