# Processo di sviluppo

### Test delle chiamate SOAP/REST dell'intermediario

Le API fornite nella documentazione dell'intermediario vengono generalmente testate in prima battuta via Postman/Insomnia nel caso di chiamate REST o via SoapUI nel caso di chiamate SOAP

### Test in ambiente locale

A seguito dei test delle chiamate, si procede con l'implementazione del microservizio. \
Per l'ambiente di sviluppo viene utilizzato Docker e Docker compose come da esempio nel seguente repository : [https://gitlab.com/opencity-labs/area-personale/protocol-proxy-sipal](https://gitlab.com/opencity-labs/area-personale/protocol-proxy-sipal)
