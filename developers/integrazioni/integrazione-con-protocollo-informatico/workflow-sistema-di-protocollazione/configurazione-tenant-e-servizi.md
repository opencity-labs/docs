---
description: >-
  Descrizione in dettaglio della fase di configurazione. Per una visione più
  approfondita delle API esposte dal Protocol Proxy, consultare la sezione
  seguente
---

# Configurazione tenant e servizi

## Panoramica

La configurazione di tenant e servizi avviene interrogando delle API apposite fornite dal Protocol Proxy di riferimento. Quest'ultimo fornisce inoltre un form come json schema sviluppato con form.io mediante il quale l'utente inserisce tali configurazioni.&#x20;

Per creare un form utilizzare il seguente builder: [https://formio.github.io/formio.js/app/builder](https://formio.github.io/formio.js/app/builder)

## Configurazione Servizio

L'admin, dall'interfaccia di configurazione dei protocolli della piattaforma compila la configurazione mediante una form, il cui json schema è servito dall'API `/v1/schema` del Protocol Proxy

<figure><img src="../../../.gitbook/assets/fakeparameters.png" alt=""><figcaption><p>Esempio interfaccia configurazione protocollo </p></figcaption></figure>



Lo schema della form soprariportata è il seguente:

```json
{
  "display": "form",
  "settings": null,
  "components": [
    {
      "label": "UUID dell'ente",
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "applyMaskOn": "change",
      "hidden": true,
      "tableView": true,
      "validate": {
        "required": true
      },
      "key": "tenant_id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "UUID del Servizio",
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "applyMaskOn": "change",
      "hidden": true,
      "tableView": true,
      "validate": {
        "required": true
      },
      "key": "id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Descrizione del servizio",
      "placeholder": "Comune di Bugliano",
      "applyMaskOn": "change",
      "hidden": true,
      "tableView": true,
      "validate": {
        "required": true
      },
      "key": "description",
      "type": "textfield",
      "input": true,
      "defaultValue": "test"
    },
    {
      "label": "Impostazioni specifiche del protocollo",
      "tableView": false,
      "validate": {
        "required": false
      },
      "key": "registry",
      "type": "container",
      "input": true,
      "components": [
        {
          "label": "URL Protocollazione",
          "placeholder": "http://www.example.com/",
          "tableView": true,
          "validate": {
            "required": true
          },
          "key": "url",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Gerarchia di Classificazione",
          "placeholder": "1.2.3",
          "tableView": true,
          "validate": {
            "required": false
          },
          "key": "classification_hierarchy",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Numero Fascicolo",
          "placeholder": "1",
          "tableView": true,
          "validate": {
            "required": false
          },
          "key": "folder_number",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Anno Fascicolo",
          "placeholder": "2022",
          "tableView": true,
          "validate": {
            "required": false
          },
          "key": "folder_year",
          "type": "textfield",
          "input": true
        }
      ]
    },
    {
      "label": "Attivo",
      "tableView": false,
      "validate": {
        "required": false
      },
      "key": "is_active",
      "type": "checkbox",
      "input": true,
      "defaultValue": false
    },
    {
      "label": "Salva",
      "tableView": false,
      "validate": {
        "required": false
      },
      "key": "submit",
      "type": "button",
      "input": true,
      "disableOnInvalid": true
    }
  ]
}
```

Premendo poi il bottone Salva, viene eseguita una `POST /services` servita dal Protocol Proxy, con payload&#x20;

```json
{
    "data": {
        "tenant_id": "",
        "id": "",
        "description": " ",
        "registry": {
            "url": "url.protocollo.it",
            "classification_hierarchy": "1.2.3",
            "folder_number": "1",
            "folder_year": "2023"
        },
        "is_active": true,
        "submit": false
    },
    "metadata": {}
}
```

Per modificare una configurazione esistente, il proxy serve l'API `PUT /services/{service_id}` e `PATCH /services/{service_id}`

Per eliminare una configurazione esistente, il proxy serve l'API `DELETE /services/{service_id}` . In questo caso l'eliminazione è una _soft-delete_, ovvero la configurazione viene semplicemente disattivata settando il parametro `active` a `false`.



## Configurazione del tenant&#x20;

la configurazione del tenant avviene in maniera nascosta durante la configurazione del servizio. Sarà la piattaforma a chiamare  le API del Protocol Proxy. La loro implementazione è descritta nella [sezione seguente](../protocol-proxy-specifiche-implementative.md#documentazione-api-swagger)&#x20;

## Alberatura configurazioni sullo storage

Le configurazioni di tenant e servizi vengono salvate con la seguente alberatura

`root`\
`|____tenants`\
`|    |____tenantid1.json`\
`|    |____tenantid2.json`\
`|    |____tenantid3.json`\
`|    |____.....`\
`|    |____tenantidn.json`\
`|____services`\
`|    |____serviced1.json`\
`|    |____serviced2.json`\
`|    |____serviced3.json`\
`|    |____.....`\
`|    |____servicedn.json`

\


