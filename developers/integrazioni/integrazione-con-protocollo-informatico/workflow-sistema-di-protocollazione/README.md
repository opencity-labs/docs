# WorkFlow sistema di protocollazione

Il Protocol Proxy interagisce con la piattaforma in due fasi distinte ma complementari:&#x20;

* Abilitazione e configurazione del sistema di protocollazione
* Protocollazione dei documenti&#x20;

### Fase 1 - abilitazione di un nuovo servizio di protocollazione per il servizio X(happy path)

<figure><img src="../../../.gitbook/assets/fase_1_workflow_CONFIG_PROTOCOL.drawio(1).png" alt=""><figcaption><p>Sequence Diagram fase di confiugurazione di un sistema di protocollo</p></figcaption></figure>

In questa fase l'amministratore della piattaforma configura un servizio, decidendo quale sistema di protocollazione abilitare per la protocollazione dei relativi servizi.&#x20;

<figure><img src="../../../.gitbook/assets/image (11).png" alt=""><figcaption><p>interfaccia di abilitazione di un protocollo esterno</p></figcaption></figure>

Cliccando sul bottone "abilita" la piattaforma contatta il Protocol proxy relativo tramite chiamate REST API. Con la prima chiamata, la piattaforma richiede la configurazione del form schema che verrà presentato all'Amministratore. Attraverso tale form è possibile  inserire le configurazioni relative al tenant e al servizio corrente. Tali configurazioni verranno inviate via  REST HTTPS  al Protocol Proxy che si occuperà della persistenza di tali configurazioni.

<figure><img src="../../../.gitbook/assets/image (10).png" alt=""><figcaption><p>Interfaccia di configurazione di un sistema di protocollazione</p></figcaption></figure>

\
Una volta che tale processo si è concluso, il Protocol Proxy è in grado, sfruttando tali configurazioni, di interagire con il sistema di protocollazione esterno.&#x20;

Le configurazioni di protocollazione possono essere create, modificate e disabilitate. Per questo motivo il Protocol Proxy deve esporre chiamate API CRUD (dettagliate nella pagina seguente).

### Fase 2 - Protocollazione dei documenti (happy path)

<figure><img src="../../../.gitbook/assets/fase2_workflow.drawio (1).png" alt=""><figcaption></figcaption></figure>

In questo diagramma, viene descritto il workflow di una pratica(ma il processo è uguale per qualsiasi altro tipo di richiesta) presentata da un utente e presa in carico da un operatore(le altre casistiche descritte nella pagina "Architettura del sistema di protocollazione" seguono lo stesso processo).

Una volta che la pratica è stata presa in carico dell'operatore essa deve essere protocollata. La piattaforma produce un evento di tipo documento sul topic "documents". Il Protocol Proxy, consuma tale evento:

* Per poter protocollare correttamente il documento, è fondamentale verificare l'esistenza delle configurazioni relative al tenant e al servizio pertinenti. Questa operazione può essere eseguita attraverso la verifica del campo "document.RemoteCollection.ID," che identifica il servizio, e "document.tenantId," che identifical'ID del tenant. Questi campi dovrebbero essere confrontati con gli ID delle configurazioni corrispondenti precedentemente salvate nello storage. Nel caso in cui tali configurazioni non siano disponibili, il documento dovrà essere semplicemente ignorato, procedendo all'evento successivo.
* &#x20;Una volta assicuratosi di dover protocollare il documento in esame, il Protocol Proxy deve verificare che il  documento non sia già stato protocollato. Un evento è protocollato quando l'oggetto "document\_number" è valorizzato con almeno il numero di protocollo. Nel caso il documento sia già stato protocollato, l'evento va semplicemente ignorato.&#x20;
* &#x20;Una volta superate le precedenti verifiche, Protocol Proxy può interagire con il sistema di protocollazione esterno per la protocollazione del documento.
* &#x20;Il Protocol Proxy produce un nuovo evento di tipo document sul topic "documents" con in aggiunta le informazioni di protocollazione(valorizzando l'oggetto "registration\_info").&#x20;
* A questo punto la piattaforma, in ascolto sullo stesso topic, provvederà ad aggiornare il suo stato interno.&#x20;

### Casi di errore - sistema di retry

In un sistema a eventi che utilizza il pattern event sourcing, è possibile che alcuni eventi non vengano processati correttamente a causa di errori temporanei come per esempio una comunicazione interrotta tra il Protocol Proxy e il sistema di protocollazione esterno. In questo caso basterebbe tentare nuovamente il consumo dello stesso evento più volte finché il problema non viene risolto.&#x20;

La piattaforma offre questo tipo di possibilità attraverso l'implementazione di un meccanismo di retry che legge da un topic(retry\_queue) specifico tutti gli eventi falliti e li reinserisce nei vari topic di origine in base a una politica di retry scelta a monte.&#x20;

Il servizio che si occupa di questo meccanismo è in grado di gestire eventi eterogenei provenienti da diversi topic. Questo è possibile solo se il servizio è a conoscenza del topic di origine in cui reinserire gli eventi da processare nuovamente.&#x20;

Per questo motivo, qualsiasi servizio si voglia avvalere del meccanismo di retry, **deve modificare l'evento fallito** inserendo il topic in cui si vuole che sia reinserito **e poi produrre l'evento** nel topic di retry impostato tramite variabile d'ambiente.

I metadati da inserire nell'evento sono nella seguente forma: \
\


```json
   "retry_meta":
   {
     "original_topic": "documents"
   }
```

Quando l'oggetto sarà reinserito nel topic "document" dal Retry Orchestrator, l'oggetto "retry\_meta" avrà dei campi aggiuntivi utili per attuare la politica di retry. Tali campi, sono gestiti e utili unicamente al Retry Orchestrator. Il Protocol Proxy non si deve quindi preoccupare di tale oggetto una volta inserito l'oggetto "retry\_meta" con il campo "original\_topic" valorizzato una tantum.&#x20;



## Interazione  Protocol Proxy <-> Sistema di protocollazione esterno e&#x20;

Solitamente la protocollazione di un documento digitale è accompagnata anche dalla protocollazione dei relativi allegati(menzionati nel paragrafo [#struttura](../documento-digitale/#struttura "mention"))\
Dal punto di vista del Protocol Proxy, la protocollazione del documento e di tutti i suoi allegati è considerata come un unica operazione atomica. Questo implica che il processo di protocollazione è considerato completo solo quando tutte le chiamate relative al documento principale e agli eventuali allegati si concludono con successo. Se anche solo una chiamata dovesse fallire, questo costituirà una condizione sufficiente per interrompere il processo di protocollazione e avviare il meccanismo di retry descritto nel paragrafo precedente. &#x20;

## Interazione Protocol Proxy<-> Piattaforma Opencity

Durante la fase di protocollazione, è possibile che il Protocol Proxy debba utilizzare le API della piattaforma per ottenere le informazioni necessarie per eseguire il processo di protocollazione. Un esempio pratico di ciò riguarda la protocollazione degli allegati. Il sistema di protocollazione richiede che il base64 del file allegato sia incluso nel payload della chiamata. Tuttavia, nel documento digitale sono presenti solo i link per scaricare i file. Pertanto, sarà compito del Protocol Proxy autenticarsi sulla piattaforma per recuperare il Token JWS e successivamente chiamare l'API specifica per ottenere il file necessario per calcolare il Base64.

Il token può essere recuperato con la seguente chiamata:

* Username : admin
* Password: admin
* Basepath: https://servizi.comune-qa.bugliano.pi.it
* Endpoint : /lang/api/auth
* method: POST&#x20;
* header Content-Type: application/json
* payload : { "username": "admin", "password": "admin" }
* Response: 200 -> `{"token": "eyDIJeiojf...."}`
* Response: 401 -> { "code": 401, "message": "Credenziali non valide." }
* Response 400 -> HTML body

Le API della piattaforma sono documentate al seguente link: \
[https://servizi.comune-qa.bugliano.pi.it/lang/api/doc](https://servizi.comune-qa.bugliano.pi.it/lang/api/doc)







