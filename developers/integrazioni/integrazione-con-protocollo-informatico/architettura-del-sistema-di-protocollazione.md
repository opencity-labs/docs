---
description: >-
  Questa pagina descrive l'architettura impiegata per attuare il processo di
  protocollazione di una pratica
---

# Architettura del sistema di protocollazione

Seguendo l'approccio del [C4Model](https://c4model.com/) viene descritta l'architettura utilizzata per attuare il processo di protocollazione. Partendo da una panoramica ad alto livello si va sempre più in dettaglio nell'architettura fino a raggiungere il secondo livello del C4 model.&#x20;

### 1 System Context

<figure><img src="../../.gitbook/assets/1SystemDiagram.drawio(2).png" alt=""><figcaption><p>C4_layer_1 arichitettura ad alto livello del sistema di protocollazione</p></figcaption></figure>

Per poter attuare il processo di protocollazione, l'area personale del cittadino si avvale dei sistemi di protocollazione esterni utilizzati dai diversi tenant. **È fondamentale notare che la piattaforma è un servizio SaaS di tipo multi-tenant.**

L’area personale del cittadino effettua diverse interazioni con il sistema di protocollo, ma tutte seguono una logica comune. Le interazioni avvengono in seguito ad alcune azioni da parte cittadini e degli operatori:

1. Invio di una pratica da parte del cittadino
2. Invio di una richiesta di integrazioni da parte di un operatore
3. Invio di una integrazione da parte del cittadino
4. Approvazione/rifiuto di una pratica

Altre azioni possibili, mediante configurazioni avanzate del servizio:

5. Creazione di una pratica da parte di un operatore per conto di un cittadino
6. Ritiro di una pratica da parte del cittadino
7. Annullamento di una pratica da parte di un operatore

**Per ognuna di queste operazioni viene prodotto un documento**. A partire dall'entità documento si costruiscono le richieste per i sistemi di protocollazioni esterni.&#x20;



### 2 Container Diagram

<figure><img src="../../.gitbook/assets/2_container_diagram.drawio(1).png" alt=""><figcaption></figcaption></figure>

Il processo di protocollazione è stato realizzato seguendo i principi del Domain Driven Design utilizzando il pattern Event Sourcing.

Di conseguenza si è scelto di separare il dominio dei servizi dal dominio dei documenti. In questo modo, i documenti sono indipendenti dalla loro fonte di origine e possono essere generati sia per esempio dalle pratiche, sia da altre strutture dati diverse senza che il sistema di protocollazione debba subire modifiche o integrazioni.

**La piattaforma è multi tenant e multi servizio:** ogni tenant può selezionare un diverso sistema di protocollazione per ogni diverso servizio che offre. Questo significa che da qualche parte la piattaforma deve  tenere traccia delle configurazioni di protocollo scelte per un determinato tenant e i servizi abilitati per il sistema scelto. Inoltre va tenuta traccia anche delle configurazioni specifiche di ogni sistema di protocollazione. \


Per tenere traccia di tali configurazioni è necessario un meccanismo di storage persistente(il cui tipo è da definire a seconda dei casi), mentre la scelta su chi detiene la responsabilità di gestire tali configurazioni è stata attribuita al Protocol Proxy.&#x20;

Questa decisione è stata dettata dal fatto che ogni sistema di protocollazione funziona in maniera diversa, e quindi ognuno di essi necessita di una configurazione specifica. Affidando la responsabilità di gestire le configurazioni al Protocol Proxy, si evita di dover modificare, per ogni nuova integrazione, il Core Application, aggirando inoltre il problema di uno sviluppo coordinato con un terzo sistema come il Protocol Proxy.

Si può dedurre quindi che esiste un mapping 1:1 tra i Protocol proxy e i sistemi di protocollazione esterni.

