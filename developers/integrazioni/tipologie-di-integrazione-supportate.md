# Tipologie di integrazione supportate

Sono possibili diversi tipi di integrazioni, che possiamo suddividere in diverse tipologie:

1. integrazioni con il flusso delle pratiche
2. integrazioni con sistemi di terze parti, come sistemi di pagamento o sistemi di protocollazione
