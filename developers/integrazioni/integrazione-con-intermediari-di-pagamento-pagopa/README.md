# Integrazione con Intermediari di pagamento PagoPA

La piattaforma consente la creazione di servizi contenenti pagamenti. I pagamenti possono essere in forma di bolli, che non richiedono una integrazione con un sistema esterno (il cittadino registra soltanto i numeri di marche da bollo che ha acquistato in precedenza), o in forma di pagamento PagoPA. \


I pagamenti PagoPA vengono creati e aggiornati mediante l'integrazione con uno degli intermediari di pagamento di PagoPA.

