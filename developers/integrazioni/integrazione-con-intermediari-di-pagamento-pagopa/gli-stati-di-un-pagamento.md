---
description: Dettaglio degli stati che attraversa un pagamento nel suo ciclo di vita
---

# Gli stati di un pagamento



<table><thead><tr><th width="267.3333333333333">STATO PAGAMENTO</th><th>DESCRIZIONE</th><th>NOTE</th></tr></thead><tbody><tr><td><code>CREATION_PENDING</code></td><td>pagamento in attesa di essere creato sull'IdP</td><td></td></tr><tr><td><code>CREATION_FAILED</code></td><td>pagamento di cui è fallita la creazione sull'IdP</td><td></td></tr><tr><td><code>PAYMENT_PENDING</code></td><td>pagamento creato sull'IdP e in attesa di essere eseguito dall'utente</td><td></td></tr><tr><td><code>PAYMENT_STARTED</code></td><td>procedura di pagamento iniziata dall'utente</td><td></td></tr><tr><td><code>COMPLETE</code></td><td>pagamento completato a seguito di conferma dalI'dP</td><td></td></tr><tr><td><code>PAYMENT_FAILED</code></td><td>pagamento fallito a causa di scadenza del termine ultimo entro cui doveva essere eseguito</td><td>nei proxy sviluppati questo stato non è quasi mai stato utilizzato</td></tr></tbody></table>

