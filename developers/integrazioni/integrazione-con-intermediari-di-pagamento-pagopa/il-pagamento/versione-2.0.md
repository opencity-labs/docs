# Versione 2.0

### JSON Evento Pagamento

```json
{
  "id": "68dada78-2398-4a11-b80a-98aaede3371c",
  "user_id": "5b00796e-ef10-4c98-b9f5-b0f7ffd7a17d",
  "type": "PAGOPA",
  "tenant_id": "60e35f02-1509-408c-b101-3b1a28109329",
  "service_id": "b21c4429-95e4-45d5-930f-44eb74136625",
  "created_at": "2023-06-06T11:59:11+02:00",
  "updated_at": "2023-06-12T00:10:16+02:00",
  "status": "PAYMENT_STARTED",
  "reason": "79501b2a-c9ad-41f8-a9e7-a885f2d570a2 - BNRMHL75C06G702B",
  "remote_id": "79501b2a-c9ad-41f8-a9e7-a885f2d570a2",
  "payment": {
    "transaction_id": null,
    "paid_at": null,
    "expire_at": "2023-09-04T11:59:05+02:00",
    "amount": 1.34,
    "currency": "EUR",
    "notice_code": "001550000000024427",
    "iud": "68dada7823984a11b80a98aaede3371c",
    "iuv": "550000000024427",
    "split": [
      {
        "code": "c_1",
        "amount": 1.00,
        "meta": {}
      },
      {
        "code": "c_2",
        "amount": 0.34,
        "meta": {}
      }
    ]
  },
  "links": {
    "online_payment_begin": {
      "url": "https://iris-proxy-qa.boat.opencontent.io/online-payment/68dada78-2398-4a11-b80a-98aaede3371c?gw=https://iristest.rete.toscana.it/gateway/PaymentAuthentication?token=1686045553436001U132&expire_at=2023-06-06T12:13:13",
      "last_opened_at": "2023-06-06T11:59:26+02:00",
      "method": "GET"
    },
    "online_payment_landing": {
      "url": "https://servizi.comune-qa.bugliano.pi.it/lang/it/pratiche/79501b2a-c9ad-41f8-a9e7-a885f2d570a2/detail",
      "last_opened_at": "2023-06-06T11:59:57+02:00",
      "method": "GET"
    },
    "offline_payment": {
      "url": "https://iris-proxy-qa.boat.opencontent.io/notice/68dada78-2398-4a11-b80a-98aaede3371c",
      "last_opened_at": "2023-06-06T11:59:20+02:00",
      "method": "GET"
    },
    "receipt": {
      "url": null,
      "last_opened_at": null,
      "method": "GET"
    },
    "notify": [
      {
        "url": "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/79501b2a-c9ad-41f8-a9e7-a885f2d570a2/payment",
        "method": "POST",
        "sent_at": null
      }
    ],
    "update": {
      "url": "http://iris-proxy-qa.boat-backplane.opencontent.io/update/68dada78-2398-4a11-b80a-98aaede3371c",
      "last_check_at": "2023-06-12T00:10:16+02:00",
      "next_check_at": "2023-06-12T01:10:16+02:00",
      "method": "GET"
    }
  },
  "payer": {
    "type": "human",
    "tax_identification_number": "BNRMHL75C06G702B",
    "name": "Michelangelo",
    "family_name": "Buonarroti",
    "street_name": "Cesare Battisti",
    "building_number": "",
    "postal_code": "38010",
    "town_name": "Bugliano",
    "country_subdivision": "PI",
    "country": "IT",
    "email": "lorenzo.bertoli@opencitylabs.it"
  },
  "event_id": "e0b25e33-9d38-4781-823a-ed04753aea01",
  "event_version": "2.0",
  "event_created_at": "2023-06-12T00:10:16+02:00",
  "app_id": "iris-payment-proxy-qa:1.2.8"
}
```

### Validazione campi

#### Payment

<table><thead><tr><th width="222">Campo</th><th width="171">Tipo</th><th width="128" data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td> <code>id</code></td><td>UUID</td><td>true</td><td></td></tr><tr><td><code>user_id</code></td><td>UUID</td><td>true</td><td></td></tr><tr><td><code>type</code></td><td>string(50)</td><td>true</td><td>Ogni proxy deve implementare un tipo di pagamento. Se per esempio, il proxy in questione gestisce pagamenti pagopa allora il campo va validato verificando che esso sia valorizzato a <code>PAGOPA</code>. In caso contrario si scarta l'evento e si emette un log di errore.</td></tr><tr><td><code>tenant_id</code></td><td>UUID</td><td>true</td><td></td></tr><tr><td><code>service_id</code></td><td>UUID</td><td>true</td><td></td></tr><tr><td><code>created_at</code></td><td>Datetime</td><td>true</td><td>ISO8601</td></tr><tr><td><code>updated_at</code></td><td>Datetime</td><td>true</td><td>ISO8601</td></tr><tr><td><code>status</code></td><td>Enum</td><td>true</td><td><p><strong>Valori permessi:</strong> <br>CREATION_PENDING CREATION_FAILED<br>PAYMENT_PENDING<br>PAYMENT_STARTED PAYMENT_CONFIRMED <br>PAYMENT_FAILED <br>NOTIFICATION_PENDING </p><p>COMPLETE<br>EXPIRED</p></td></tr><tr><td><code>reason</code></td><td>string(140)</td><td>true</td><td>Causale del pagamento. Non può eccedere i 140 caratteri.</td></tr><tr><td><code>remote_id</code></td><td>UUID</td><td>true</td><td></td></tr><tr><td><code>payment</code></td><td>PaymentData</td><td>true</td><td></td></tr><tr><td><code>links</code></td><td>Links</td><td>true</td><td></td></tr><tr><td><code>payer</code></td><td>Payer</td><td>true</td><td></td></tr><tr><td><code>event_id</code></td><td>UUID</td><td>true</td><td></td></tr><tr><td><code>event_version</code></td><td>string(10)</td><td>true</td><td>L'attuale versione dell'evento deve essere "1.0". Ogni proxy deve validare la versione dell'evento in modo da sapere se processarlo o meno.</td></tr><tr><td><code>event_created_at</code></td><td>Datetime</td><td>true</td><td>ISO8601</td></tr><tr><td><code>app_id</code></td><td>string(100)</td><td>true</td><td>Valorizzarlo nel formato <code>&#x3C;nome-proxy>:&#x3C;versione-proxy></code><br>Es.<br><code>iris-payment-proxy:1.3.0</code></td></tr></tbody></table>

#### PaymentData

<table><thead><tr><th width="204">Campo</th><th width="226">Tipo</th><th width="135" data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td><code>transaction_id</code></td><td>string(255)</td><td>false</td><td>Fornito dall'intermediario di pagamento. La lunghezza dunque può essere variabile</td></tr><tr><td><code>paid_at</code></td><td>Datetime</td><td>false</td><td>ISO8601</td></tr><tr><td><code>expire_at</code></td><td>Datetime</td><td>true</td><td>ISO8601<br><br>E' già valorizzato a priori, quindi non viene gestito dal proxy</td></tr><tr><td><code>amount</code></td><td>float</td><td>true</td><td>E' già valorizzato a priori, quindi non viene gestito dal proxy</td></tr><tr><td><code>currency</code></td><td>string(3)</td><td>true</td><td>ISO4217</td></tr><tr><td><code>notice_code</code></td><td>string(50)</td><td>false</td><td></td></tr><tr><td><code>iud</code></td><td>string(50)</td><td>true</td><td></td></tr><tr><td><code>iuv</code></td><td>string(50)</td><td>false</td><td></td></tr><tr><td><code>split</code></td><td>List[PaymentDataSplit]</td><td>false</td><td></td></tr></tbody></table>

#### PaymentDataSplit

<table><thead><tr><th width="200">Campo</th><th>Tipo</th><th data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td>code</td><td>string(50)</td><td>true</td><td></td></tr><tr><td>amount</td><td>float</td><td>false</td><td></td></tr><tr><td>meta</td><td>json</td><td>false</td><td></td></tr></tbody></table>

#### Links

<table><thead><tr><th width="277">Campo</th><th width="201">Tipo</th><th data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td><code>online_payment_begin</code></td><td>UrlData</td><td>true</td><td></td></tr><tr><td><code>online_payment_landing</code></td><td>UrlData</td><td>true</td><td></td></tr><tr><td><code>offline_payment</code></td><td>UrlData</td><td>true</td><td></td></tr><tr><td><code>receipt</code></td><td>UrlData</td><td>true</td><td></td></tr><tr><td><code>notify</code></td><td>List[Notify]</td><td>false</td><td></td></tr><tr><td><code>update</code></td><td>Update</td><td>true</td><td></td></tr></tbody></table>

#### UrlData

<table><thead><tr><th>Campo</th><th>Tipo</th><th width="170" data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td><code>url</code></td><td>string</td><td>false</td><td></td></tr><tr><td><code>last_opened_at</code></td><td>Datetime</td><td>false</td><td>ISO8601</td></tr><tr><td><code>method</code></td><td>Enum</td><td>false</td><td><strong>Valori permessi:</strong><br>GET<br>POST</td></tr></tbody></table>

#### Notify

<table><thead><tr><th>Campo</th><th>Tipo</th><th width="138" data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td><code>url</code></td><td>string</td><td>false</td><td></td></tr><tr><td><code>method</code></td><td>Enum</td><td>false</td><td><strong>Valori permessi:</strong><br>GET<br>POST</td></tr><tr><td><code>sent_at</code></td><td>Datetime</td><td>false</td><td>ISO8601</td></tr></tbody></table>

#### Update

<table><thead><tr><th>Campo</th><th>Tipo</th><th width="154" data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td><code>url</code></td><td>string</td><td>false</td><td></td></tr><tr><td><code>last_check_at</code></td><td>Datetime</td><td>false</td><td>ISO8601</td></tr><tr><td><code>next_check_at</code></td><td>Datetime</td><td>false</td><td>ISO8601</td></tr><tr><td><code>method</code></td><td>Enum</td><td>false</td><td><strong>Valori permessi:</strong><br>GET<br>POST</td></tr></tbody></table>

#### Payer

<table><thead><tr><th width="306">Campo</th><th width="143">Tipo</th><th width="131" data-type="checkbox">Obbligatorio</th><th>Validazione</th></tr></thead><tbody><tr><td><code>type</code></td><td>Enum</td><td>true</td><td><strong>Valori permessi:</strong><br>human<br>legal</td></tr><tr><td><code>tax_identification_number</code></td><td>string(255)</td><td>true</td><td></td></tr><tr><td><code>name</code></td><td>string(255)</td><td>true</td><td></td></tr><tr><td><code>family_name</code></td><td>string(255)</td><td>false</td><td></td></tr><tr><td><code>street_name</code></td><td>string(255)</td><td>false</td><td></td></tr><tr><td><code>building_number</code></td><td>string(255)</td><td>false</td><td></td></tr><tr><td><code>postal_code</code></td><td>string(255)</td><td>false</td><td></td></tr><tr><td><code>town_name</code></td><td>string(255)</td><td>false</td><td></td></tr><tr><td><code>country_subdivision</code></td><td>string(2)</td><td>false</td><td>ISO 3166-2</td></tr><tr><td><code>country</code></td><td>string(2)</td><td>false</td><td>ISO 3166-1 alpha-2</td></tr><tr><td><code>email</code></td><td>string(255)</td><td>false</td><td></td></tr></tbody></table>
