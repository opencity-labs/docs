# Configurazione tenant e servizi

## Panoramica

La configurazione di tenant e servizi avviene interrogando delle API apposite fornite dal proxy di riferimento. Quest'ultimo fornisce inoltre un form come json schema sviluppato con form.io mediante il quale l'utente inserisce tali configurazioni.&#x20;

Per creare un form utilizzare il seguente builder: [https://formio.github.io/formio.js/app/builder](https://formio.github.io/formio.js/app/builder)

## Configurazione tenant

L'admin, dall'interfaccia di configurazione dei pagamenti della Stanza del Cittadino compila la configurazione mediante una form, il cui json schema è servito dall'API `/tenants/schema`

<figure><img src="../../.gitbook/assets/image (3).png" alt=""><figcaption></figcaption></figure>

Lo schema della form sopra riportata è il seguente

```json
{
  "display": "form",
  "components": [
    {
      "label": "UUID del Tenant",
      "hidden": true,
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "spellcheck": false,
      "attributes": {
        "readonly": "readonly"
      },
      "validate": {
        "required": true
      },
      "key": "id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Intestatario del conto",
      "placeholder": "Comune di Bugliano",
      "validate": {
        "required": true
      },
      "key": "name",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codice fiscale/Partita IVA dell'ente",
      "placeholder": "A000",
      "spellcheck": false,
      "key": "tax_identification_number",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codice iban dell'ente",
      "placeholder": "IT60 X054 2811 1010 0000 0123 456",
      "spellcheck": false,
      "key": "iban",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codice ente",
      "placeholder": "0000000",
      "description": "Codice dell'ente fornito da Efil",
      "spellcheck": false,
      "validate": {
        "required": true
      },
      "key": "code",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Identificativo dell’applicazione",
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "description": "Deve coincidere con l'utenza autorizzata su Efil",
      "spellcheck": false,
      "validate": {
        "required": true
      },
      "key": "api_key",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codice applicazione",
      "spellcheck": false,
      "key": "application_code",
      "clearOnHide": false,
      "type": "textfield",
      "input": true,
      "hidden": true,
      "defaultValue": "SDC"
    },
    {
      "label": "Abilitato",
      "key": "active",
      "type": "checkbox",
      "input": true,
      "hidden": true,
      "defaultValue": true
    },
    {
      "label": "Salva",
      "showValidations": false,
      "key": "submit",
      "type": "button",
      "input": true
    }
  ]
}
```

Premendo poi il bottone Salva, viene eseguita una `POST /tenants` servita dal proxy, con payload&#x20;

```json
{
  "name": "string",
  "tax_identification_number": "string",
  "iban": "string",
  "code": "string",
  "api_key": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "application_code": "string",
  "active": true,
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
}
```

Per modificare una configurazione esistente, il proxy serve l'API `PUT /tenants/{tenant_id}` e `PATCH /tenants/{tenant_id}`

Per eliminare una configurazione esistente, il proxy serve l'API `DELETE /tenants/{tenant_id}` . In questo caso l'eliminazione è una _soft-delete_, ovvero la configurazione viene semplicemente disattivata settando il parametro `active` a `false` ed eliminando la configurazione dalla memoria ma non dallo storage.

## Configurazione servizio

L'admin, dall'interfaccia di configurazione dei pagamenti per un servizio compila la configurazione mediante una form, il cui json schema è servito dall'API `/services/schema`

<figure><img src="../../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

Lo schema della form soprariportata è il seguente

```json
{
  "display": "form",
  "components": [
    {
      "label": "UUID del Servizio",
      "hidden": true,
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "spellcheck": false,
      "attributes": {
        "readonly": "readonly"
      },
      "validate": {
        "required": true
      },
      "key": "id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "UUID del Tenant",
      "hidden": true,
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "spellcheck": false,
      "attributes": {
        "readonly": "readonly"
      },
      "validate": {
        "required": true
      },
      "key": "tenant_id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codice del servizio",
      "placeholder": "0000000",
      "description": "Codice del servizio fornito da efil",
      "validate": {
        "required": true
      },
      "key": "code",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Descrizione",
      "placeholder": "Servizi accessori CIE",
      "description": "Descrizione del servizio",
      "validate": {
        "required": true
      },
      "key": "description",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codici di accertamento",
      "description": "Lista dei codici di accertamento associabili al servizio",
      "multiple": true,
      "defaultValue": [
        ""
      ],
      "key": "available_split_codes",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Tipologia di dovuto",
      "placeholder": "Avviso, Bolletta, Verbale, Fattura",
      "description": "Specificare la tipologia del dovuto che si sta caricando (Es: Avviso, Bolletta, Verbale, Fattura, etc)",
      "spellcheck": false,
      "validate": {
        "required": true,
        "maxLength": 35
      },
      "key": "management_id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Codice iban dell'ente",
      "placeholder": "IT60 X054 2811 1010 0000 0123 456",
      "spellcheck": false,
      "key": "iban",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Abilitato",
      "key": "active",
      "type": "checkbox",
      "input": true,
      "hidden": true,
      "defaultValue": true
    },
    {
      "label": "Bilancio",
      "tableView": false,
      "key": "split_enable",
      "type": "checkbox",
      "input": true,
      "defaultValue": true
    },
    {
      "label": "Bilancio",
      "reorder": false,
      "addAnotherPosition": "bottom",
      "layoutFixed": false,
      "enableRowGroups": false,
      "initEmpty": false,
      "tableView": false,
      "defaultValue": [
        {
          "split_id": "",
          "split_code": "",
          "split_amount": 0
        }
      ],
      "key": "split",
      "conditional": {
        "show": true,
        "when": "split_enable",
        "eq": "true"
      },
      "type": "datagrid",
      "input": true,
      "components": [
        {
          "label": "ID",
          "description": "Identificativo univoco della voce di bilancio. Testo libero o uguale al codice.",
          "placeholder": "2020/1",
          "tableView": true,
          "validate": {
            "required": true
          },
          "key": "split_id",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Codice",
          "description": "Questo campo deve essere uno di quelli della lista di codici di accertamento associabili al servizio",
          "placeholder": "2020/1",
          "tableView": true,
          "redrawOn": "available_split_codes",
          "validate": {
            "required": true,
            "custom": "if (input && !data.available_split_codes.includes(input)){\n  valid = \"Codice non configurato nella lista dei codici di accertamento disponibili\"\n}"
          },
          "key": "split_code",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Importo",
          "description": "Importo della voce di bilancio. NB: La somma degli importi delle voci DEVE equivalere all'importo totale",
          "placeholder": "16.00",
          "validate": {
            "required": true
          },
          "mask": false,
          "tableView": false,
          "delimiter": false,
          "requireDecimal": false,
          "inputFormat": "plain",
          "truncateMultipleSpaces": false,
          "key": "split_amount",
          "type": "number",
          "input": true
        }
      ]
    },
    {
      "label": "hidden",
      "calculateValue": "if (!data.split || data.split == 'undefined') {\n  data.split = []\n} else if (typeof data.split==='object' && Object.keys(data.split).length === 0) {\n  data.split = [];\n}",
      "key": "hidden",
      "type": "hidden",
      "input": true,
      "tableView": false
    },
    {
      "label": "Salva",
      "showValidations": false,
      "key": "submit",
      "type": "button",
      "input": true
    }
  ]
}
```

Premendo poi il bottone Salva, viene eseguita una `POST /services` servita dal proxy, con payload&#x20;

```json
{
  "tenant_id": "60e35f02-1509-408c-b101-3b1a28109329",
  "code": "0000087",
  "description": "Abbonamento aree di sosta",
  "available_split_codes": [
    "2022/1",
    "2022/2"
  ],
  "split": [],
  "management_id": "TEST",
  "iban": "IT49C0200823307000028474373",
  "active": true,
  "id": "daa0f528-b582-4d1c-9691-e226ac443424"
}
```

Per modificare una configurazione esistente, il proxy serve l'API `PUT /services/{service_id}` e `PATCH /services/{service_id}`

Per eliminare una configurazione esistente, il proxy serve l'API `DELETE /services/{service_id}` . In questo caso l'eliminazione è una _soft-delete_, ovvero la configurazione viene semplicemente disattivata settando il parametro `active` a `false`.

## Alberatura configurazioni sullo storage

Le configurazioni di tenant e servizi vengono salvate con la seguente alberatura

`root`\
`|____tenant_id_1`\
`|    |____tenant.json`\
`|    |____service_id_1.json`\
`|    |____service_id_2.json`\
`|    |____.....`\
`|    |____service_id_n.json`\
`|____tenant_id_2`\
`|    |____tenant.json`\
`|    |____service_id_1.json`\
`|    |____service_id_2.json`\
`|    |____.....`\
`|    |____service_id_n.json`\
`|____tenant_id_n`\
&#x20;    `|____tenant.json`\
&#x20;    `|____service_id_1.json`\
&#x20;    `|____service_id_2.json`\
&#x20;    `|____.....`\
&#x20;    `|____service_id_n.json`

