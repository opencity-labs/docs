---
description: >-
  Molti aspetti implementativi dipendono più dalla normativa che da nostre
  scelte progettuali, in questa pagina si evidenziano gli uni e le altre.
---

# Implementazione di un proxy

### Requisiti funzionali

Sono requisiti che dipendono da nostre scelte implementative, possono cambiare man mano che evolviamo la piattaforma, per supportare nuovi casi d'uso (pagamento dei dovuti) oppure perché ci accorgiamo che abbiamo commesso qualche errore nella progettazione.

1. Gestione di pagamenti spontanei
2. Gestione di pagamenti con bilancio
3. Gestione della notifica dello stato del pagamento da parte dell'IdP - se disponibile - o del polling se non è disponibile la notifica
4. Se è implementato il polling, questo deve avvenire con un [back-off](https://cloud.google.com/memorystore/docs/redis/exponential-backoff?hl=it) esponenziale che assicuri alcuni check nel giro di pochi minuti e poi si diradi fino a un massimo di un check al giorno fino alla scadenza del pagamento o 1 anno se la scadenza è inferiore.
5. Gestione delle configurazioni a livello di Tenant e di Servizio

### Requisiti normativi

* Avere una specifica in formato OpenAPI v3
* Rispettare la [Linee Guida di interoperabilità](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs): in particolare per quanto riguarda&#x20;
  * [il formato dei dati](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs/it/bozza/doc/04\_Raccomandazioni%20di%20implementazione/04\_raccomandazioni-tecniche-generali/02\_formato-dei-dati.html) (4.2)
  * [raccomandazioni sul naming](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs/it/bozza/doc/04\_Raccomandazioni%20di%20implementazione/04\_raccomandazioni-tecniche-generali/03\_progettazione-e-naming.html) (4.3) optando per la scelta di `snake_case` per i nomi degli attributi
  * logging (4.4)
  * risultare valido nel [validatore ufficiale OA3](https://github.com/italia/api-oas-checker)

### Requisiti per l'ambiente di produzione

* Sappiamo che in molti topic ci sono diversi eventi mal formati, per vari errori e assenza di uno schema registry, quindi è necessario validare gli eventi in input e opzionalmente in output (lo fanno le nostre prime implementazioni in python, ma è un requisito?)
* Ci aspettiamo che se di dovesse aggiornare il formato dell'evento nel topic `payments` gestiremo eventi anche in parallelo con versioni distinte, quindi ogni proxy deve Interpretare solo gli eventi che riportano il formato più recente dell'evento nel topic payments: `event_version: 2.0`
* Integrarsi con [sentry](https://sentry.io/) per la gestione degli errori
* Esporre metriche di monitoraggio in [formato prometheus](https://prometheus.io/docs/concepts/data\_model/), in particolare:
  * counter sul numero di pagamenti creati
  * counter sul numero di errori durante il processamento di pagamenti, differenziando gli errori interni (errori di formato dati in ingresso o validazione) da errori esterni (errori di dialogo con l'IdP)
  * latenza delle chiamate fatte all'IdP

Inoltre il servizio deve rispettare gli [standard della piattaforma](../../standard-e-convenzioni/standard-della-piattaforma.md) in particolare per quanto riguarda la gestione dello storage: non possiamo avere vendor-lockin sulle tecnologie, quindi dobbiamo essere compatibili con file-system posix tradizionale, NFS share e almeno i due cloud-storage più diffusi  S3 e Azure Blob.

### Esempio di Swagger di un proxy di pagamento

{% embed url="https://api.qa.stanzadelcittadino.it/payment-proxy/iris/docs" %}

### Esempi di proxy da cui si può prendere spunto

EFIL: [http://gitlab.com/opencontent/stanza-del-cittadino/efil-payment-proxy](http://gitlab.com/opencontent/stanza-del-cittadino/efil-payment-proxy)

IRIS: [http://gitlab.com/opencontent/stanza-del-cittadino/iris-payment-proxy](http://gitlab.com/opencontent/stanza-del-cittadino/efil-payment-proxy)

MYPAY: [http://gitlab.com/opencontent/stanza-del-cittadino/mypay-payment-proxy](http://gitlab.com/opencontent/stanza-del-cittadino/efil-payment-proxy)

PMPAY: [http://gitlab.com/opencontent/stanza-del-cittadino/pmpay-payment-proxy](http://gitlab.com/opencontent/stanza-del-cittadino/efil-payment-proxy)

Questi proxy condividono alcune scelte implementative grazie a un apposito python-sdk sviluppato in parallelo ai proxy stessi:

* le configurazioni sono salvate su disco locale o s3 secondo un albero ben definito tenant->servizio
* i singoli pagamenti vengono salvati su storage fino a che il pagamento è pendente, così da poter fare il polling in autonomia.



Altri riferimenti utili

* [Presentazione API Interoperability a EuroPython 2018](https://docs.italia.it/italia/piano-triennale-ict/lg-modellointeroperabilita-docs/it/bozza/doc/04\_Raccomandazioni%20di%20implementazione/04\_raccomandazioni-tecniche-generali/03\_progettazione-e-naming.html)
* [Presentazione API Interoperability a EuroPython 2019](https://docs.google.com/presentation/d/1blql0E\_zcbq7r-wzmslgJPiW7ELkYlIn9\_fqIVEXr4A/edit#slide=id.p4)
* [API Starter Kit](https://github.com/teamdigitale/api-starter-kit)

