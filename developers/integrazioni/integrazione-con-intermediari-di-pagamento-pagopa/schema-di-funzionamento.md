---
description: >-
  Panoramica dei ciclo di funzionamento dei pagamenti di Opencity Italia - Area
  Personale.
---

# Schema di Funzionamento

### Creazione di un pagamento <a href="#_blojbv6bcvp3" id="_blojbv6bcvp3"></a>

Un pagamento nasce da una interazione con il cittadino: il cittadino presenta una pratica per la quale l'Ente richiede un pagamento.&#x20;

<figure><img src="../../.gitbook/assets/image (9).png" alt=""><figcaption><p>Rispetto al Core della piattaforma, i pagamenti sono gestiti mediante un insieme dedicato di microservizi</p></figcaption></figure>

Il cuore dell'integrazione con un intermediario di pagamento è un microservizio apposito che dialoga con l'intermediario da un lato, implementando le chiamate specifiche in protocollo ReST o SOAP, e con il core della piattaforma dall'altro lato, mediante la lettura e la scrittura su un topic di Kafka.

## Componenti coinvolte <a href="#_kcesosjbxtbn" id="_kcesosjbxtbn"></a>

### Componenti del core della piattaforma

In questo momento i pagamenti hanno una relazione 1 a 1  con le pratiche, di conseguenza ci sono due servizi che fanno da intermediari tra le pratiche e i pagamenti, ovvero il **payment dispatcher** e il **payment updater**.

#### Core <a href="#_638lt9qllzxx" id="_638lt9qllzxx"></a>

E’ l’interfaccia dalla quale l’utente usufruisce di un servizio digitale, inserendo i dati di una pratica attraverso un modulo: se il servizio prevede un pagamento, i dati di questo sono presenti nella descrizione JSON della pratica e saranno utilizzati dal dispatcher per creare il pagamento.

#### Payment dispatcher <a href="#_tczsm2oy8g23" id="_tczsm2oy8g23"></a>

E’ il microservizio che, a partire dall'evento di una pratica nel topic `applications`, crea l'evento del pagamento nel topic `payments.`

#### Payment updater

Il microservizio legge gli eventi dal topic `payments` e contestualmente aggiorna lo stato della pratica cambiando lo stato da Payment pending a Inviata.

### Sottosistema dei pagamenti

#### Payment proxy <a href="#_nocwclr2372n" id="_nocwclr2372n"></a>

E' il microservizio che gestisce la comunicazione e l’aggiornamento della posizione debitoria con l'Intermediario di pagamento di riferimento. Esso gestisce inoltre il salvataggio delle configurazioni tramite le quali può interagire con l'intermediario di pagamento di riferimento. Queste configurazioni sono inserite dall'operatore mediante l'interfaccia del Core, il quale comunica con il payment proxy mediante delle apposite API che quest'ultimo serve.

#### Ksql <a href="#_cmv5g99t7bbk" id="_cmv5g99t7bbk"></a>

E' un microservizio che ascolta dal topic payments e crea un database interrogabile con SQL contenente tutti i pagamenti in stati non definitivi.

#### Payments poller <a href="#_cmv5g99t7bbk" id="_cmv5g99t7bbk"></a>

E' il microservizio che monitora lo stato dei pagamenti: periodicamente legge su KSQL l'elenco dei pagamenti aperti e invia per ognuno di questi una chiamata alla "update.url" specificata nel pagamento stesso: questa url è una url del proxy stesso che consente al proxy di interrogare l'Intermediario per avere lo stato aggiornato del pagamento.
