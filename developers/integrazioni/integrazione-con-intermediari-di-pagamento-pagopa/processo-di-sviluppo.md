# Processo di sviluppo

### Test delle chiamate SOAP/REST dell'intermediario

Le API fornite nella documentazione dell'intermediario vengono generalmente testate in prima battuta via Postman/Insomnia nel caso di chiamate REST o via SoapUI nel caso di chiamate SOAP

### Test in ambiente locale

A seguito dei test delle chiamate, si procede con l'implementazione del microservizio. Per testarlo localmente fare riferimento al seguente `docker-compose.yml`

<pre><code>version: '2'

services:
  zookeeper:
    image: confluentinc/cp-zookeeper:7.0.0
    hostname: zookeeper
    ports:
      - "2181:2181"
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000

  kafka:
    image: confluentinc/cp-kafka:7.0.0
    hostname: kafka
    depends_on:
      - zookeeper
    ports:
      - "29092:29092"
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:9092,PLAINTEXT_HOST://localhost:29092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 0
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
      KAFKA_CREATE_TOPICS: "applications,payments_cache"

  kafka-ui:
    image: provectuslabs/kafka-ui:latest
    ports:
      - "8080:8080"
    depends_on:
      - kafka
    environment:
      KAFKA_CLUSTERS_0_NAME: local
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: kafka:9092
      # KAFKA_CLUSTERS_0_ZOOKEEPER: zookeeper:2181
      KAFKA_CLUSTERS_0_KSQLDBSERVER: ksqldb-server:8088

  topic-init:
<strong>    image: lorello/alpine-bash:1.4.0    
</strong><strong>    depends_on:      
</strong><strong>      - kafka    
</strong><strong>    volumes:      
</strong><strong>      - ./init.d:/data    
</strong><strong>    command:      
</strong><strong>      - /bin/bash      
</strong><strong>      - -c      
</strong><strong>      - |          
</strong><strong>          wait-for-it kafka:9092          
</strong><strong>          kaf config add-cluster local -b kafka:9092 &#x26;&#x26; \          
</strong><strong>          kaf config use-cluster local          
</strong><strong>          set -ex          
</strong><strong>          for I in /data/payment*.json; do            
</strong><strong>            echo "Importing $$I"            
</strong><strong>            cat $$I | jq -c | kaf produce payments          
</strong><strong>          done
</strong>
  # la configurazione varia in base al proxy da implementare
  iris-proxy:
    build:
      context: .
    ports:
      - "8000:8000"
    environment:
      CANCEL_PAYMENT: "false"
      KAFKA_TOPIC_NAME: "payments"
      KAFKA_BOOTSTRAP_SERVERS: "kafka:9092"
      KAFKA_GROUP_ID: "iris_payment_proxy"
      IRIS_OTF_PAYMENT_URL: "https://apistage.regione.toscana.it/C01/ComunicazionePosizioniDebitorieOTF/v3/IdpAllineamentoPendenzeEnteOTF"
      IRIS_IUV_URL: "https://iristest.rete.toscana.it/IdpBillerNdpServices/GenerazioneIUVService"
      IRIS_OUTCOME_URL: "https://apistage.regione.toscana.it/C01/InvioNotificaPagamentoEsito/v3/IdpInformativaPagamento.Esito"
      IRIS_VERIFY_URL: "https://apistage.regione.toscana.it/C01/VerificaStatoPagamento/v3/IdpVerificaStatoPagamento"
      IRIS_NOTICE_URL: "https://iristest.rete.toscana.it/IdpBillerNdpServices/GenerazioneAvvisiService"
      BASE_PATH_CONFIG: "sdc-payments/iris/tenants/"
      BASE_PATH_EVENT: "sdc-payments/iris/payments/"
      STORAGE_TYPE: "MINIO"
      STORAGE_KEY: "AKIAIOSFODNN7EXAMPLE"
      STORAGE_SECRET: "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
      STORAGE_BUCKET_NAME: "payments"
      STORAGE_ENDPOINT: "minio:9000"
    depends_on:
      - createbuckets
      - kafka

  # storage dove vengono salvate le configurazioni del tenant, del servizio e lo stato dei vari pagamenti
  minio:
    image: minio/minio
    command: server /data --console-address ":9001"
    restart: unless-stopped
    volumes:
      - ./bucket:/data
    ports:
      - 9000:9000
      - 9001:9001
    environment:
      MINIO_ACCESS_KEY: AKIAIOSFODNN7EXAMPLE
      MINIO_SECRET_KEY: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

  createbuckets:
    image: minio/mc
    depends_on:
      - minio
    entrypoint: >
        /bin/sh -c "
        while ! /usr/bin/mc config host add minio http://minio:9000 AKIAIOSFODNN7EXAMPLE wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY;
        do echo 'Wait minio to startup...' &#x26;&#x26; sleep 0.1; done;
        /usr/bin/mc mb minio/payments;
        /usr/bin/mc policy set public minio/payments;
        exit 0;
        "

</code></pre>

### Test in ambiente QA

I pagamenti sono testabili sul nostro ambiente di qa: [https://servizi.comune-qa.bugliano.pi.it/](https://servizi.comune-qa.bugliano.pi.it/).&#x20;

Si potrà accedere come utente SPID utilizzando l'utente AGID TEST DEMO.

Si potrà accedere come admin per eventualmente modificare il servizio di test e la relativa configurazione di pagamento

Si potrà accedere come operatore per testare pagamenti posticipati, i quali richiedono la presa in carico della pratica e l'approvazione di quest'ultima affinchè l'utente possa procedere con il pagamento.

\
\
