# Requisiti per l'integrazione

L’area personale del cittadino effettua diverse interazioni con l'intermediario di pagamento, ma tutte seguono una logica comune. Le interazioni avvengono in seguito ad alcune azioni da parte dei cittadini e degli operatori:

* Invio di una pratica da parte del cittadino
* Approvazione di una pratica da parte di un operatore nel caso di un pagamento posticipato
* Importazione di dovuti da parte dell'operatore mediante file csv
* Pagamento di un dovuto da parte del cittadino a seguito dell'importazione di quest'ultimo

### Flusso di interazione tra Area Personale e sistema di pagamento

Per ogni integrazione sono necessarie solitamente le seguenti chiamate API o webservice:

1. Creazione di una posizione debitoria sull'intermediario di pagamento
2. Recupero del link per pagare online la posizione debitoria creata allo step 1
3. Recupero dell'avviso di pagamento cartaceo
4. Verifica dello stato di un pagamento, questo può avvenire in due modi:
   1. Polling periodico verso l'intermediario di pagamento
   2. Invio di notifica da parte dell'intermediario verso l'area personale chiamando un url apposito comunicato a priori
5. Recupero della ricevuta di pagamento quando questo è completato

### Processo di integrazione

1. Condivisione documentazione:
   1. Documentazione delle API o dei WebService
   2. ambiente di test (endpoint, credenziali di accesso, tipo dovuto, codice servizio ecc.)
   3. Esempio di chiamate necessarie per creare una posizione debitoria
2. Test delle chiamate al sistema di pagamento effettuate in modo manuale
3. Implementazione e rilascio dell’integrazione in ambiente di Quality Assurance
4. Validazione su un servizio di test
5. Rilascio in ambiente di produzione
