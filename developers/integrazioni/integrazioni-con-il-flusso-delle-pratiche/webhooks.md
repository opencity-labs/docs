---
description: >-
  Sono un meccanismo di comunicazione tra sistemi che consentono una
  comunicazione in tempo reale tra due API
---

# Webhooks

Le API non sono sufficienti per realizzare una buona integrazione tra due sistemi. Se due o più sistemi si devono aggiornare reciprocamente in base agli eventi che avvengono al proprio interno, è necessario che periodicamente ogni sistema interroghi gli altri per sapere se il loro stato è cambiato: questo approccio si chiama anche _polling_. Questo è molto oneroso e introduce un ritardo nell'aggiornamento che è tanto maggiore quanto maggiore è l'intervallo con cui un sistema interroga gli altri. D'altra parte se di intensifica la frequenza del polling si avranno maggiori costi a fronte di aggiornamenti magari poco frequenti.

Un modo più efficiente di far dialogare due sistemi è avviare una comunicazione in occasione di cambiamenti rilevanti per il sistema in ascolto e per ottenere questo, eliminando del tutto la necessità di fare _polling_, è possibile usare i Webhook.

Ogni webhook consente di inviare un aggiornamento  a una API di un altro sistema che sta ascolto, in base agli eventi che si desidera comunicare.

![Esempio di webhook che un amministratore può configurare dal pannello di amministrazione della piattaforma](https://lh4.googleusercontent.com/e8exe7gAkCIrDo\_juCe8vuZ5pE7gZgMFWfK\_fROBlBNIkfKD0yZ65HMrklHda34nrUp0L3DH8yBkKbmpHOw54zGvHwSOgkknOxLqtQX70WLZAsWHKqh2h2eKLvJaH-8owANzR1yOLPDGRYw5ye6YcxuBkQ=s2048)

![Per ogni webhook è possibile stabilire un endpoint e quali eventi di quali servizi devono essere comunicati](https://lh6.googleusercontent.com/UtwPk5jvScC-tzrIp7qJPOeaQO0XYcnzfCZduBeuMYgHXEKXrSaE\_URtx\_lGNIUo-3itJ1xB1H0T8b17plvQeA4G3\_gzJzLahxOS9rc55yIXOQT4Z8eylLSoJ2Cyjt3ISXVSH9fKeWsPMwtXb5qDOj6zRQ=s2048)

Per fare test sui webhook si consiglia di utilizzare un servizio come [Request Inspector](https://link.opencitylabs.it/request-inspector?utm\_source=opencitylabs\&utm\_medium=gitbook).

