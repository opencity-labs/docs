# Integrazioni con il flusso delle pratiche

La piattaforma può offrire sia una esperienza completa al cittadino o all'impresa, che consente l'invio di pratiche e la loro gestione nel backend, sia farsi carico di uno solo di questi aspetti, lasciando ad altri sistemi il ruolo di interfaccia con il cittadino o a sistemi verticali il compito di gestire pratiche (pratiche edilizie o in ambito sociale)

In presenza di questa esigenza è importante che la piattaforma e il sistema terzo siano in grado di instaurare  e mantenere nel tempo la relazione tra le pratiche, mantenenere aggiornati lo stato della pratica o di un pagamento pendente per la stessa.

Vari strumenti nella piattaforma consentono di mantenere aggiornate le informazioni e ricevere aggiornamenti:

1. API ReST
2. Webhook

Prima di entrare nei dettagli di questi strumenti merita avere uno sguardo di insieme del flusso della pratica e di tutti gli stati che può attraversare. Nel grafico che segue il tratto continuo più forte rappresenta il flusso più frequente di una pratica, il tratto punteggiato rappresenta i flussi alternativi. I cerchi costituiscono gli stati finali di una pratica

```mermaid
---
title: Flusso di una pratica
---

graph LR
    Bozza==>Sent[Inviata]
    Sent==>|PDF|Acquisita
    Sent-.->Retired((Ritirata))
    Acquisita==>|?|Protocollata
    Acquisita-.->Retired((Ritirata))
    Protocollata==>InCarico
    Acquisita-.->InCarico
    InCarico==>Accepted((Approvata))
    InCarico-.->Refused((Rifiutata))
    InCarico-.->Cancelled((Annullata))
```

```mermaid
---
title: Flusso di una pratica con pagamento anticipato
---

graph LR
    Bozza==>PaymentPending[In attesa di pagamento]
    PaymentPending==>Payed[Pagata]
    PaymentPending-->PaymentFailed[Pagamento fallito]
    PaymentFailed[Pagamento fallito]-->Cancelled
    Payed[Pagata]==>Sent[Inviata]
    Sent==>|PDF|Acquisita
    Sent-.->Retired((Ritirata))
    Acquisita==>|?|Protocollata
    Acquisita-.->Retired((Ritirata))
    Protocollata==>InCarico
    Acquisita-.->InCarico
    InCarico==>Accepted((Approvata))
    InCarico-.->Refused((Rifiutata))
    InCarico-.->Cancelled((Annullata))
```

```mermaid
---
title: Flusso di una pratica con pagamento posticipato
---

graph LR
    Bozza==>Sent[Inviata]
    Sent==>|PDF|Acquisita
    Sent-.->Retired((Ritirata))
    Acquisita==>|?|Protocollata
    Acquisita-.->Retired((Ritirata))
    Protocollata==>InCarico
    Acquisita-.->InCarico
    InCarico==>PaymentPending[In attesa di pagamento]
    PaymentPending==>Payed[Pagata]
    PaymentPending-->PaymentFailed[Pagamento fallito]
    PaymentFailed[Pagamento fallito]-->Cancelled
    Payed[Pagata]==>Accepted((Approvata))

    InCarico-.->Refused((Rifiutata))
    InCarico-.->Cancelled((Annullata))
```
