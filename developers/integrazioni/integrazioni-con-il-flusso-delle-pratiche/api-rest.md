---
description: >-
  Le API consentono di leggere e modificare informazioni sulla piattaforma in
  modo programmatico.
---

# API ReST

Una API ReST, nota anche come API RESTful, è un'interfaccia di programmazione delle applicazioni (API o API web) conforme ai vincoli dello stile architetturale ReST, che consente l'interazione con servizi web RESTful. Il termine REST, coniato dall'informatico Roy Fielding, è l'acronimo di REpresentational State Transfer.

La documentazione delle API ReST della piattaforma è pubblicamente disponibile in ogni Ente all'indirizzo:

{% embed url="https://dominio/PATH/api/docs" %}
Indirizzo della documentazione delle API
{% endembed %}

Ad esempio per il comune demo della piattaforma è possibile consultarle alla pagina:

{% embed url="https://servizi.comune.bugliano.pi.it/lang/api/doc" %}
Documentazione API
{% endembed %}

### Autenticazione

Alcune API richiedono un'autenticazione prima di essere chiamate, in questo caso un cittadino o un operatore possono usare le proprie credenziali per ottenere un token di autenticazione come segue:



{% swagger method="post" path="/auth" baseUrl="https://servizi.comune.bugliano.pi.it/lang/api" summary="Richiesta token di autenticazione" %}
{% swagger-description %}
Con questa chiamata si riceve un token di autenticazione in formato JWT, che dovrà essere inviato per tutte le successive chiamate che richiedono autenticazione come _bearer token_ nell'header _Autorization_
{% endswagger-description %}

{% swagger-parameter in="body" name="username" required="true" %}
nome utente
{% endswagger-parameter %}

{% swagger-parameter in="body" name="password" required="true" %}
password
{% endswagger-parameter %}

{% swagger-response status="200: OK" description="Se le credenziali sono corrette si riceverà un token di autenticazione" %}

{% endswagger-response %}
{% endswagger %}
