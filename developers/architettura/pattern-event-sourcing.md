---
description: >-
  Un'architettura guidata dagli eventi è costituita da producer eventi che
  generano un flusso di eventi e consumer eventi che sono in ascolto degli
  eventi.
---

# Pattern: event sourcing

Streaming eventi: gli eventi vengono scritti in un log. Gli eventi sono rigorosamente ordinati (in una partizione) e durevoli. I client non sottoscrivono il flusso, ma un client può invece leggere da qualsiasi parte del flusso. Il client è responsabile di far avanzare la propria posizione nel flusso. Questo significa che un client può aggiungersi in qualsiasi momento e può riprodurre gli eventi.

<figure><img src="https://lh5.googleusercontent.com/6NFecGqZ4ULohHFGfpkrcjwyXnx3v_k8AbYe_4jHi9yWoC54h35OlueGt2v3I-rzC76NQyufiqBiTvbQLlLwFXKO1tNxwvncFo2ZYhwI46vDNi-vP_4AtQcFxfqcHMw-QEJS3ZHmn-4Z7t1iMiHwCf6l4w=s2048" alt=""><figcaption></figcaption></figure>

Elaborazione del flusso di eventi. Usare una piattaforma di flussi di dati come Apache Kafka, come pipeline per inserire gli eventi e fornirli agli elaboratori di flussi. Gli elaboratori di flussi intervengono per elaborare o trasformare il flusso. Possono essere presenti più elaboratori di flussi per sottosistemi diversi nell'applicazione.\
