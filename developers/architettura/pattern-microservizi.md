---
description: >-
  Un'architettura di microservizi è costituita da un insieme di servizi ridotti
  autonomi.
---

# Pattern: microservizi

Ogni servizio è autonomo e deve implementa una singola funzionalità all'interno di un contesto delimitato. Un contesto delimitato è una divisione naturale all'interno di un Ente e fornisce un limite esplicito all'interno del quale esiste un modello di dominio.

Ogni microservizio della piattaforma è distribuito come docker container, ha un proprio versionamento ed espone una o più porte per comunicare con l'esterno.

I microservizi sono esposti all'esterno da un web router che assolve solitamente anche il ruolo di terminatore di SSL.

<figure><img src="https://lh6.googleusercontent.com/ObAXwgZ9Rsj6TAWmsBMzDv_neFuuj7q1GUHD8ELPOKyKB6m5Qhjy5jeoBCPyALqsrO0vIJ_FAb1yT6HeYzSUfZ1YNz2A7vtO5kIlhIlNJHZG2F_3aeBaz36FDFAJl8Up0QGDJpj1rNhuxbrSV4fosZ8jxA=s2048" alt=""><figcaption></figcaption></figure>

Ad ogni release della piattaforma vengono aggiornati uno o più microservizi.\
\


\
