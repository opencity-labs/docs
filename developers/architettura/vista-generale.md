---
description: >-
  Il sistema è costituito da un core, che assolve ai compiti principali e da
  alcuni sottosistemi dedicati a compiti specifici: gestione dei pagamenti,
  protocollazione, sistema di analytics.
---

# Vista generale

<figure><img src="../.gitbook/assets/image (8).png" alt=""><figcaption><p>Organizzazoine generale del sistema</p></figcaption></figure>

### Core

Il componente principale, il core, è ovviamente il frutto del lavoro dei primi anni di sviluppo della piattaforma, ed assolve ai compiti principali della stessa:

1. Definizione e organizzazione dei Servizi Digitali
2. Definizione dei Moduli on-line
3. Gestione delle Pratiche inviate dai cittadini e dei messaggi
4. Gestione degli utenti
5. Gestione Uffici e appuntamenti on-line su slot di tempo e orari di apertura.
6. Gestione Sale pubbliche e altre risorse a prenotazione flessibile



Il core assolve anche al compito di interfaccia utente, sia per i cittadini che per gli operatori e gli amministratori degli Enti.

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption><p>Servizi che costituiscono il core</p></figcaption></figure>

{% hint style="info" %}
All'interno del core sono presenti alcune integrazioni con sistemi di protocollo e di intermediari di pagamento PagoPA: si tratta delle prime integrazioni realizzate quando lo sviluppo era di tipo monolitico, successivamente sono stati creati i sottosistemi dedicati alle integrazioni con Protocolli e Pagamenti. In futuro probabilmente queste integrazioni verranno riscritte come microservizi indipendenti, alleggerendo le responsabilità del core e guadagnandone anche la semplicità del codice e la sua manutenibilità.
{% endhint %}

Il core è costituito dai seguenti microservizi:

* **Caddy / Symfony App** (dal 2023 i due servizi sono distribuiti in una immagine unica: `registry.gitlab.com/opencontent/stanza-del-cittadino/core/app`)\
  Nota
* **Form Server**, implementazione del server di Form.IO: `registry.gitlab.com/opencontent/stanza-del-cittadino/form-server`
* **Form Builder**, un editor multi-tenant con accesso riservato agli amministratori della piattaforma per l'editing dei componenti condivisi dei form degli Enti: `registry.gitlab.com/opencontent/stanza-del-cittadino/formbuilderjs`
* **Varnish**, un cache proxy server che incrementa l'affidabilità e la scalabilità del formserver e del catalogo dei servizi: `registry.gitlab.com/opencontent/varnish:latest`
* **Gotenberg**, a Docker-powered stateless API for PDF files: `gotenberg/gotenberg:7`
* **Payment Updates,** ascolta gli eventi relativi ai pagamenti e aggiorna le pratiche di conseguenza: `registry.gitlab.com/opencontent/stanza-del-cittadino/payment-updater`
* **Signature Check**, api usata dal componente sdc\_upload di form.io per validate le firme dei documenti firmati .p7m: `geopartner/signcheckwebapi:latest`

Per un esempio completo di deploy dei microservizi si veda il file [`docker-compose.yml`](https://gitlab.com/opencontent/stanza-del-cittadino/core/-/blob/master/docker-compose.yml) nel repositoy del core, che fornisce una configurazione funzionante della piattaforma.

{% hint style="info" %}
L'immagine **Caddy / Symfony App** viene usata sia per esporre la UI e le API, sia per l'esecuzione dei cron jobs, per un esempio si veda il file `docker-compose` usato in ambiente di sviluppo..
{% endhint %}

Utilizza inoltre i seguenti servizi di persistenza:

* **PostgreSQL**, per la gestione di tutti i dati e delle configurazioni della piattaforma, ad eccezione delle form. La versione usata in sviluppo è la 11 e comprende anche l'estensione PostGIS: `postgis/postgis:11-3.3-alpine`
* **MongoDB**, per la persistenza dei moduli realizzati con l'editor di Form.IO: `mongo:4.2`
* **Redis**, per la gestione delle sessioni degli utenti e della cache applicativa: `redis:5-alpine.`



