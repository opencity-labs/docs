---
description: >-
  Questo documento è rivolto a sviluppatori, tecnici e Partner Tecnologici  ed è
  volto a fornire informazioni tecniche sull'architettura, le integrazioni, le
  interfacce disponibili nella piattaforma.
layout: landing
---

# Introduzione

La piattaforma OpenCity Italia semplifica la realizzazione e la gestione di servizi digitali per cittadini e imprese, anche da smartphone (art. 7 CAD).

OpenCity Italia è open source, scaricabile da [Developers Italia](https://developers.italia.it/it/search?type=all\_catalogue\&sort\_by=relevance\&search\_value=opencity\&page=0) o disponibile come servizio pronto all'uso (Software as a Service).

La piattaforma è sviluppata con l'obiettivo di erogare servizi per centinaia di Enti contemporaneamente da una singola installazione, per questo motivo il codice della piattaforma è organizzato in microservizi indipendenti tra loro e il dialogo tra gli stessi è realizzato mediante _stream_ di eventi piuttosto che mediante chiamate dirette alle API.

Per queste scelte l'installazione e la gestione della piattaforma in un ambiente di produzione richiede un ampio ventaglio di conoscenze:

* Sistema Operativo GNU/Linux e uso della _command line_ (CLI)
* database PostgresQL, MongoDB, Redis
* Docker e sistemi di orchestrazione di container
* Kafka e KsqlDB

Per contribuire alla piattaforma, a seconda dell'area su cui si desidera sviluppare sono necessarie cometenze nei seguenti linguaggi di programmazione:

* PHP
* NodeJS
* Python
* Golang

Infine la piattaforma è in costante evoluzione e nuovi componenti vengono aggiunti o prendono il posto di componenti esistenti divenuti obsoleti.

