# Table of contents

* [Introduzione](README.md)
* [Architettura](architettura/README.md)
  * [Pattern: microservizi](architettura/pattern-microservizi.md)
  * [Pattern: event sourcing](architettura/pattern-event-sourcing.md)
  * [Vista generale](architettura/vista-generale.md)
* [Standard e convenzioni](standard-e-convenzioni/README.md)
  * [Standard della piattaforma](standard-e-convenzioni/standard-della-piattaforma.md)
  * [Microservizi](standard-e-convenzioni/microservizi.md)

## Integrazioni

* [Tipologie di integrazione supportate](integrazioni/tipologie-di-integrazione-supportate.md)
* [Integrazioni con il flusso delle pratiche](integrazioni/integrazioni-con-il-flusso-delle-pratiche/README.md)
  * [API ReST](integrazioni/integrazioni-con-il-flusso-delle-pratiche/api-rest.md)
  * [Webhooks](integrazioni/integrazioni-con-il-flusso-delle-pratiche/webhooks.md)
* [Integrazione con Intermediari di pagamento PagoPA](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/README.md)
  * [Requisiti per l'integrazione](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/requisiti-per-lintegrazione.md)
  * [Il Pagamento](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/il-pagamento/README.md)
    * [Versione 1.0](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/il-pagamento/versione-1.0.md)
    * [Versione 2.0](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/il-pagamento/versione-2.0.md)
  * [Schema di Funzionamento](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/schema-di-funzionamento.md)
  * [Configurazione tenant e servizi](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/configurazione-tenant-e-servizi.md)
  * [Un pagamento in dettaglio](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/un-pagamento-in-dettaglio.md)
  * [Gli stati di un pagamento](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/gli-stati-di-un-pagamento.md)
  * [Processo di sviluppo](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/processo-di-sviluppo.md)
  * [Implementazione di un proxy](integrazioni/integrazione-con-intermediari-di-pagamento-pagopa/implementazione-di-un-proxy.md)
* [Integrazione con Protocollo Informatico](integrazioni/integrazione-con-protocollo-informatico/README.md)
  * [Requisiti per l'integrazione](integrazioni/integrazione-con-protocollo-informatico/requisiti-per-lintegrazione.md)
  * [Documento digitale](integrazioni/integrazione-con-protocollo-informatico/documento-digitale/README.md)
    * [Usecase: Il Documento originato dalle pratiche dai servizi digitali](integrazioni/integrazione-con-protocollo-informatico/documento-digitale/usecase-il-documento-originato-dalle-pratiche-dai-servizi-digitali.md)
  * [Architettura del sistema di protocollazione](integrazioni/integrazione-con-protocollo-informatico/architettura-del-sistema-di-protocollazione.md)
  * [WorkFlow sistema di protocollazione](integrazioni/integrazione-con-protocollo-informatico/workflow-sistema-di-protocollazione/README.md)
    * [Configurazione tenant e servizi](integrazioni/integrazione-con-protocollo-informatico/workflow-sistema-di-protocollazione/configurazione-tenant-e-servizi.md)
  * [Protocol Proxy: Specifiche Implementative](integrazioni/integrazione-con-protocollo-informatico/protocol-proxy-specifiche-implementative.md)
  * [Processo di sviluppo](integrazioni/integrazione-con-protocollo-informatico/processo-di-sviluppo.md)

## 👩💻 Sviluppo

* [Temi grafici](sviluppo/temi-grafici.md)
