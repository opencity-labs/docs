---
description: >-
  L'aggiunta di un tema grafico richiede una modifica al Core della piattaforma,
  in queste pagine si da indicazione dei temi presenti e di come predisporre una
  Merge-Request per aggiungerne uno nuovo.
---

# Temi grafici

### Come creare un MR per aggiungere un tema

Step preparativi alla modifica:

* Clonare il repository [https://gitlab.com/opencity-labs/area-personale/core](https://gitlab.com/opencity-labs/area-personale/core/)&#x20;
* Posizionarsi sul branch master
* Creare un nuovo branch di nome add-theme-$themename e spostarsi sul nuovo branch con il comando `git checkout -b "add-theme-$themename"` sostituendo con il nome del nuovo tema che si vuole creare (per le indicazioni sui nomi dei temi vedere sotto)
* Creare una MR sul branch che effettui squash di tutti i commit del branch ed elimini il branch una volta mergiato come di seguito:

&#x20;

<figure><img src="../.gitbook/assets/immagine.png" alt=""><figcaption></figcaption></figure>

Successivamente creare un nuovo file in `./assets/styles` copiando il contenuto di seguito che dovrebbe coincidere con il file `default.scss.` Il nome del file deve coincidere con il nome che avrà il tema e avere l'estensione .scss (l nome del tema può contenere solo lettere e il carattere \`-\`, deve rispondere cioè alla espressione regolare \[a-z]\[a-z0-9-]+. Es: se si vuole creare il tema "amaranto" aggiungiamo un file `amaranto.scss`):

```scss
@charset "UTF-8";

$primary: #3478bd;

$primary-a5: $primary;
$primary-a6: $primary;
$primary-a7: $primary;

// Header
$header-slim-bg-color: #2b649e;     // Colore di background del top header
$header-slim-button-color: #2b649e; // Colore di background del bottone di accesso all'area personale
$header-slim-button-hover-color: $primary; // Colore di background del bottone di accesso all'area personale su hover

$header-center-bg-color: #3478bd;       // Colore di background del header centrale
//$header-center-text-color: #fff; // Colore del testo del header centrale

// Footer
$bg-footer: #16334f; // Colore di background del footer

// Content
$card-link-color: $primary; // Colore link all'interno delle card

@import "core";

```

* Nel file cambiare le opzioni ed i colori a proprio piacimento
* Si compila il nuovo tema su webpack aggiungendo nel file [webpack.config.js](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/webpack.config.js) StyleEntry come di seguito: `.addStyleEntry('amaranto', './assets/styles/amaranto.scss')` (Il nome deve coincidere con il nome del file creato in precedenza). I colori devono tenere conto della linea guida Accessibilità.
* Si aggiunge alla piattaforma il nuovo tema aggiornando nel file [src/Model/Tenant/ThemeOptions.php](https://gitlab.com/opencity-labs/area-personale/core/-/blob/master/src/Model/Tenant/ThemeOptions.php) la costante `THEMES` e inserendo il nuovo tema come di seguito: `'Amaranto' => 'amaranto'`
